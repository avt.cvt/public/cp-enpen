#!/usr/bin/env python3
import sys
from py.ImpedancePlot import *

name = sys.argv[1] if len(sys.argv) > 1 else "pedot_kcl"

ip = ImpedancePlot('outdata/' + name)

ip.nyquist()
ip.bode()

plt.show()
