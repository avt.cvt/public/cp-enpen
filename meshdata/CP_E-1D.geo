lc = 1/10; // one micrometer
lc_fine_internal = lc/10000;
lc_fine_boundary = lc/10000;
lc_cp = lc/1;
lc_electrolyte = lc;
h = 3; // length of refinement
d_cp = 18.0;
//cell_thickness = 2800;
//electrolyte_thickness = cell_thickness - 2*d_cp;
//conc_electrolyte = 0.1;
//d_el = electrolyte_thickness / 2 + 7.2 * electrolyte_thickness * conc_electrolyte; // last part substitutes ito resistance for given conc_electrolyte

d_el = 2500.0 + 24000 / 10*2; // last part substitutes ito resistance for 0.5M * actual conc


Point(1) = {0.0, 0.0, 0.0, lc_fine_boundary};
Point(2) = {h,0,0,lc_cp};
Point(3) = {d_cp-h, 0.0, 0.0, lc_cp};
Point(4) = {d_cp, 0.0, 0.0, lc_fine_internal};
Point(5) = {d_cp+h, 0.0, 0.0, lc_electrolyte};
Point(6) = {d_cp+10*h, 0.0, 0.0, lc_electrolyte*100};
Point(7) = {d_cp+d_el, 0.0, 0.0, lc_electrolyte*1000};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};

Physical Line ("CPLEFT") = {1,2,3};
Physical Line ("EL") = {4,5,6};