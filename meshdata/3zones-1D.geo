// mesh element size
lc = 1/10; // one micrometer
lc_fine_internal = lc/10000;
lc_fine_boundary = lc/100000;
lc_pedot = lc/100;
lc_membrane = lc*5;
h = 0.02; // length of refinement
thickness = 0.1; // boundary layer thickness [1mu]
memb_thickness = 200.0; // membrane thickness [1mu]
Point(1) = {0.0, 0.0, 0.0, lc_fine_boundary};
Point(2) = {h,0,0,lc_pedot};
Point(3) = {thickness-h, 0.0, 0.0, lc_pedot};
Point(4) = {thickness, 0.0, 0.0, lc_fine_internal};
Point(5) = {thickness+h, 0.0, 0.0, lc_membrane};
Point(6) = {thickness+memb_thickness-h, 0.0, 0.0, lc_membrane};
Point(7) = {thickness+memb_thickness, 0.0, 0.0, lc_fine_internal};
Point(8) = {thickness+memb_thickness+h, 0.0, 0.0, lc_pedot};
Point(9) = {thickness+memb_thickness+thickness-h, 0.0, 0.0, lc_pedot};
Point(10) = {thickness+memb_thickness+thickness, 0.0, 0.0, lc_fine_boundary};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,9};
Line(9) = {9,10};

Physical Line ("LEFT") = {1,2,3};
Physical Line ("CENTER") = {4,5,6};
Physical Line ("RIGHT") = {7,8,9};