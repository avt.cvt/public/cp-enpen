lc = 1/10; // one micrometer
lc_fine_internal = lc/10000;
lc_fine_boundary = lc/10000;
lc_cp = lc/1;
lc_electrolyte = lc;
h = 0.02; // length of refinement
d_cp = 0.4;
d_el = 3000; // last part substitutes ito resistance for 0.5M * actual conc
Point(1) = {0.0, 0.0, 0.0, lc_fine_boundary};
Point(2) = {h,0,0,lc_cp};
Point(3) = {d_cp-h, 0.0, 0.0, lc_cp};
Point(4) = {d_cp, 0.0, 0.0, lc_fine_internal};
Point(5) = {d_cp+h, 0.0, 0.0, lc_electrolyte};
Point(6) = {d_cp+10*h, 0.0, 0.0, lc_electrolyte*100};
Point(7) = {d_cp+d_el, 0.0, 0.0, lc_electrolyte*1000};
Point(8) = {2*d_el + d_cp - 10*h, 0.0, 0.0, lc_electrolyte*100};
Point(9) = {d_cp + 2*d_el - h, 0.0, 0.0, lc_electrolyte};
Point(10) = {d_cp + 2*d_el, 0.0, 0.0, lc_fine_internal};
Point(11) = {d_cp + 2*d_el + h, 0.0, 0.0, lc_cp};
Point(12) = {d_cp + 2*d_el + d_cp - h,0,0,lc_cp};
Point(13) = {d_cp + 2*d_el + d_cp, 0.0, 0.0, lc_fine_boundary};

// Point(8) = {d_cp+d_el + 10, 0.0, 0.0, lc_electrolyte*100}; //{2*d_el + d_cp - 10*h, 0.0, 0.0, lc_electrolyte*100};
// Point(9) = {d_cp+d_el + 20, 0.0, 0.0, lc_electrolyte}; // {d_cp + 2*d_el - h, 0.0, 0.0, lc_electrolyte};
// Point(10) = {d_cp+d_el + 30, 0.0, 0.0, lc_fine_internal}; // {d_cp + 2*d_el, 0.0, 0.0, lc_fine_internal};
// Point(11) = {d_cp+d_el + 40, 0.0, 0.0, lc_cp}; // {d_cp + 2*d_el + h, 0.0, 0.0, lc_cp};
// Point(12) = {d_cp+d_el + 50,0,0,lc_cp}; // {d_cp + 2*d_el + d_cp - h,0,0,lc_cp};
// Point(13) = {d_cp+d_el + 60, 0.0, 0.0, lc_fine_boundary}; // {d_cp + 2*d_el + d_cp, 0.0, 0.0, lc_fine_boundary};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,9};
Line(9) = {9,10};
Line(10) = {10,11};
Line(11) = {11,12};
Line(12) = {12,13};


Physical Line ("CPLEFT") = {1,2,3};
Physical Line ("ELLEFT") = {4,5,6};
Physical Line ("ELRIGHT") = {7,8,9};
Physical Line ("CPRIGHT") = {10,11,12};