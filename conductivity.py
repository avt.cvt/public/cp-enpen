#!/usr/bin/env python3
import sys
from py.Npp1DPlots import *

simulation = sys.argv[1] if len(sys.argv) > 1 else "vDBPulses"

d = Npp1DPlots('outdata/' + simulation)

d.showConductivity()
d.data.exportConductivity()

plt.show()