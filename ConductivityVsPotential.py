#!/usr/bin/env python3
import sys
import matplotlib as mpl
import os

from py.Npp1DPlots import *

cdir = os.path.dirname(os.path.realpath(__file__))

mpl.style.use('seaborn-white')
plt.style.use(cdir + '/py/cvt.mplstyle')

simulation = sys.argv[1] if len(sys.argv) > 1 else "ConductivityOverPotential"

d = Npp1DPlots(cdir + '/outdata/' + simulation)

d.showConductivityOverPotential()

plt.show()