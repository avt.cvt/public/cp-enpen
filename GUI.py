#!/usr/bin/env python3
from PyQt5.uic import loadUi, loadUiType
import sys
from PyQt5 import QtGui
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import (
    FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from PyQt5.QtWidgets import QApplication, QMainWindow, QSlider
from py.NppPlot1D import NppPlot1D as nppp,plt
import os

simulation = sys.argv[1] if len(sys.argv) > 1 else "shuttle_init"
d = nppp('outdata/' + simulation)

class Main(QMainWindow):
    def __init__(self):
        super().__init__()
        loadUi(os.path.dirname(__file__) + '/py/GUI.ui', self)

        self.conc_fig_dict = {}
        self.flux_fig_dict = {}
        self.conc_plot_dict ={}
        self.flux_plot_dict ={}

        self.steps = d.get_steps()
        
        self.conc_view_lst.itemClicked.connect(self.change_concfig)
        self.flux_view_lst.itemClicked.connect(self.change_fluxfig)
        self.slider.valueChanged[int].connect(self.set_num)        
        self.slider.setMinimum(0)
        self.slider.setMaximum(self.steps)
        self.slider.setValue(0)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.setTickInterval(1)
        
        self.set_btn.clicked.connect(self.on_click)
        
        self.min_label.setNum(0)
        self.max_label.setNum(self.steps)                
        
        self.slider.valueChanged[int].connect(lambda: self.update_Plots())
        
        self.num_line.setValidator(QtGui.QIntValidator())
        
        fig = Figure()
        self.addmpl(fig)
    
    def initial_view(self):
            self.conc_shown = True
            self.flux_shown = False
            self.text = next(iter(self.conc_fig_dict))
            self.rmmpl()
            self.addmpl(self.conc_fig_dict[self.text])
            self.update_Plots()
       
    def on_click(self):
        step = int(self.num_line.text())
        self.slider.setValue(step)
    
    def update_Plots(self):
        if self.slider.value()==0 or self.slider.value()==self.steps:
            pass
        if self.conc_shown == True:
            plot = self.conc_plot_dict[self.text]
            plt.clf()
            figure = self.conc_fig_dict[self.text]
            ax = figure.add_subplot(111)
            plot = d.animate_s(plot, d.names.index(self.text), self.slider.value())
            ax.relim()
            ax.autoscale_view()
            self.conc_plot_dict[d.names.index(self.text)] = plot        
            figure.canvas.draw()
        elif self.flux_shown == True:
            plot = self.flux_plot_dict[self.text]
            plt.clf()
            figure = self.flux_fig_dict[self.text]
            ax = figure.add_subplot(111)
            plot = d.flux_update(plot, d.names.index(self.text), self.slider.value())
            ax.relim()
            ax.autoscale_view()
            self.flux_plot_dict[d.names.index(self.text)] = plot        
            figure.canvas.draw()
           
    def set_num(self):
        num =self.slider.value()
        num = str(num)
        self.num_line.setText(num)
        
    def change_concfig(self, item):
        self.conc_shown = True
        self.flux_shown = False
        self.text = item.text()
        self.rmmpl()
        self.addmpl(self.conc_fig_dict[self.text])
        self.update_Plots()
    
    def change_fluxfig(self, item):
        self.conc_shown = False
        self.flux_shown = True
        self.text = item.text()
        self.rmmpl()
        self.addmpl(self.flux_fig_dict[self.text])
        self.update_Plots()    
    
    def addmpl(self, fig):
        self.canvas = FigureCanvas(fig)
        self.mplvl.addWidget(self.canvas)
        self.canvas.draw()
        self.toolbar = NavigationToolbar(self.canvas, self.mplwindow, coordinates=True)
        self.mplvl.addWidget(self.toolbar)
        
    def add_conc_fig(self, name, fig):
        self.conc_fig_dict[name] = fig
        self.conc_view_lst.addItem(name)
    
    def add_conc_plot(self,i, plot):
        self.conc_plot_dict[i] = plot
        
    def add_flux_fig(self,name, fig):
        self.flux_fig_dict[name] = fig
        self.flux_view_lst.addItem(name)
        
    def add_flux_plot(self,i, plot):
        self.flux_plot_dict[i] = plot
            
    def rmmpl(self,):
        self.mplvl.removeWidget(self.canvas)
        self.canvas.close()
        self.toolbar.close()
        
if __name__ == '__main__':

    app = QApplication(sys.argv)
    main = Main()
    for i in range(0,d.nv):
        fig_conc = Figure(figsize=(10, 8))
        ax_conc = fig_conc.add_subplot(111)
        plot_conc = d.animate_state_GUI(i, ax_conc, 0)
        main.add_conc_fig(d.names[i], fig_conc)
        main.add_conc_plot(d.names[i], plot_conc)
        
        fig_flux = Figure(figsize=(10, 8))
        ax_flux = fig_flux.add_subplot(111)
        plot_flux = d.flux_evolution(i, ax_flux, 0)    
        main.add_flux_fig(d.names[i], fig_flux)
        main.add_flux_plot(d.names[i], plot_flux)

    main.initial_view()

    main.show()

    sys.exit(app.exec_()) 
