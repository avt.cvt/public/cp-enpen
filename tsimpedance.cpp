#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif
#include <petscsys.h>

#include "src/ParameterStore.h"
#include "src/Application.h"
#include "src/Models/TybrandtNPP.h"
#include "src/Models/TybrandtShuttleNPP.h"
#include "src/Mesh/Mesh1D.h"
#include "src/ResultWriter/ResultWriter.h"
#include "src/ResultWriter/ResultLoader.h"
#include <string>
#include <map>
#include "math.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <list>

#include "src/Models/BVReaction.h"

int main(int argc, char **argv)
{
    PetscErrorCode ierr;
    ierr = PetscInitialize(&argc, &argv, NULL, NULL);
    CHKERRXX(ierr);

    /* Set up simulation */
    ParameterStore p("sysdata/tybrandt-shuttle.def");

    TybrandtShuttleNPP model;

    Application app(&p, (Model *)&model);
    unsigned int n = app.nv;

    BVReaction shuttle(p.num_species,3,4);
    shuttle.k_0 = 1e-5;
    shuttle.E0 = -8;

    Mesh1D mesh("meshdata/CP_EL_CP-200-1D.msh", n);

    double cel = 1e-1;
    double cSox = 1e-3;
    double cSred = 1e-5;

    double initial[] = {0, cel, cel - cSred, cSox, cSred, 0, 0}; // Values normalized to M
    mesh.set_state(&initial[0], n);

    Zone *cpleft_zone = mesh.getZone("CPLEFT");
    cpleft_zone->activeStates.assign({0,1,2,3,4,5,6});
    cpleft_zone->charge = -1;
    cpleft_zone->Bmod = 1.24;
    cpleft_zone->alpha = 0.25;
    cpleft_zone->applyToVolumes();

    Zone *elleft_zone = mesh.getZone("ELLEFT");
    elleft_zone->activeStates.assign({1,2,3,4,6});
    elleft_zone->applyToVolumes();

    Zone *elright_zone = mesh.getZone("ELRIGHT");
    elright_zone->activeStates.assign({1,2,3,4,6});
    elright_zone->applyToVolumes();

    Zone *cpright_zone = mesh.getZone("CPRIGHT");
    cpright_zone->activeStates.assign({0,1,2,3,4,5,6});
    cpright_zone->charge = -1;
    cpright_zone->Bmod = 1.24;
    cpright_zone->alpha = 0.25;
    cpright_zone->applyToVolumes();

    mesh.initialize();
    app.setMesh((Mesh *)&mesh);

    mesh.set_boundary_state(FACETYPE_BOUNDARY_RIGHT, &initial[0], n);
    mesh.set_boundary_state(FACETYPE_BOUNDARY_LEFT, &initial[0], n);


    int bcond_cp[] = {3, 2, 2, 2, 2, 0, 2};
    mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_cp[0]);
    mesh.setBoundaryCondition(cpright_zone->getRightBoundary(), &bcond_cp[0]);

    // cpright_zone->bvreaction = &shuttle;
    // cpright_zone->updateVolumeProperties();
    // cpleft_zone->bvreaction = &shuttle;
    // cpleft_zone->updateVolumeProperties();

    ResultLoader rl(&app);
    rl.load("outdata/InitialState/state/0");

    double frequency = (argc > 2 && atof(argv[2]) > 0) ? atof(argv[2]) : 1e3;
    ResultWriter eisrw("outdata/TSImpedance/ts_eis_" + std::to_string(frequency), &app);
    app.impedance(frequency,0.001);
    app.rw = &eisrw;
    app.impedance(frequency,0.001);
    eisrw.close();

    /* Finish and exit */
    app.destroy();
    return PetscFinalize();
}
