#ifndef APPLICATION_H
#define APPLICATION_H

#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif
#include <petscsys.h>

#include "Mesh/Mesh.h"
#include "ParameterStore.h"

class Application;
class ResultWriter;
class Model;
class SolverData;
#include "ResultWriter/ResultWriter.h"
#include "Solver.h"
#include "Models/Model.h"

#include <iostream>
#include <string>
#include <algorithm>
#include <cmath>
#include <string>

class Application {

public:
	ParameterStore *p;
	Mesh *mesh = NULL;
	SolverData *solverdata;
	Model *model;
	unsigned int nv;
	ResultWriter* rw;
	double CVScanRate = 0.1;
	double CVAmplitude = 1;
	double EISFrequency = 1e3;
	double EISAmplitude = 4;
	double EISt0 = 0;
	double EISBias = 0;
	double droppingResistance = 300; // Ohm
	double appliedVoltage = 0; // thermal Volt
	double current = 0.6;
	double deviceVoltage = 0;
	double pulse = 0; 
	std::string mode = "";

	Application(ParameterStore *p, Model *model);
	void setMesh(Mesh* mesh);
	void pseudoTransient(double final_time = 4e3);
	void transient(double duration,double dt);
	void adaptiveTransient(double duration,double dt);
	void transientWithResistance(double duration,double dt, double R, double V);
	
	void cyclicVoltammetry(double scanRate=4, double amplitude=40); // Input in thermal Volt (40 thermal Volt = 1 Volt)
	void cyclicVoltammetry_WR(double scanRate=4, double amplitude=40, double R=0);
	void destroy();
	void impedance(double frequency=1e3, double amplitude=4, double bias=0);
	double getOCP(int index);
	~Application();
};

#endif