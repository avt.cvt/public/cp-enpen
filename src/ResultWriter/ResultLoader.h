#ifndef RESULTLOADER_H
#define RESULTLOADER_H

#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif

class ResultLoader;
class Application;
#include "../Application.h"
#include "../Mesh/Mesh.h"

#include <sys/types.h>
#include <sys/stat.h>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

#include <iostream>
#include <fstream>

class ResultLoader {
private:
    Application* app;

public:
    ResultLoader(Application* app): app(app){};
    bool load(std::string file);
};


#endif