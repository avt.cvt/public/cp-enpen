#include "ResultLoader.h"

bool ResultLoader::load(std::string file){

    std::ifstream statefile(file);
    if(statefile.fail()) return false;

    Mesh* mesh = this->app->mesh;

    std::string line;
    double val;

    for(unsigned int i = 0; i < mesh->num_volumes; i++) {
        std::getline(statefile, line);
        std::stringstream ss(line);
        for(unsigned int j = 0;j < this->app->nv;j++){
            if(ss.peek() != ',') {
                ss >> val;
                mesh->volumes[i]->state[j] = val;
            }
            ss.ignore();
        }
    }

    return true;
}