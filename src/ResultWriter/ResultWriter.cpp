#include "ResultWriter.h"
#include <iostream>
#include <iomanip>
#include "math.h"

PetscMPIInt ResultWriter::MPIrank(){
    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    return rank;
}


ResultWriter::ResultWriter(std::string dir, Application* app){
    if(this->MPIrank() != 0) return;

    this->app = app;
    this->dir = dir;
    
    if(mkdir(dir.c_str(), 0777) && (errno =! EEXIST)) 
        throw std::runtime_error("Could not create output dir");

    system(("exec mkdir -p " + dir).c_str());

    system(("exec rm -rf " + dir +"/*").c_str());
    mkdir((dir + "/state").c_str(), 0777);
    mkdir((dir + "/flux").c_str(), 0777);

    std::ofstream& js = this->json;
    js.open(dir + "/index.json");
    js << std::setprecision(18);

    js << "{" << '\n' << "\"nv\":" << this->app->nv << ",\n\"species\":[";
    for(unsigned int i = 0; i < this->app->p->num_species; i++)
        js << '"' << this->app->p->name[i] << '"' << ',';
    js << "\"phi_cp\",\"phi\"],\n" << "\"charges\":[";
    for(unsigned int i = 0; i < this->app->p->num_species; i++) {
        js << '"' << this->app->p->z[i] << '"';
        if(i < this->app->p->num_species-1) js << ',';
    }
    js << "],\n";

    // print zones
    Mesh* mesh = this->app->mesh;
    js << "\"zones\":{\n";
    for(Zone* z: mesh->zones){
        Node& a = z->bounds.a;
        Node& b = z->bounds.b;
        js << '"' << z->name << '"' << ": {\"a\": [";
        js << a.x << ',' << a.y << ',' << a.z;
        js << "], \"b\":[";
        js << b.x << ',' << b.y << ',' << b.z;
        js << "]}";
        if(z != mesh->zones.back()) js  << ",\n";
    }
    js << "},\n";


    js << "\"indices\":{\n";

    this->write_volumes();
    this->write_faces();
}

void ResultWriter::close(){
    if(this->MPIrank() != 0) return;

    this->json << "\n}}";
    this->json.close();
}

void ResultWriter::write_state(double indexval){
    if(this->MPIrank() != 0) return;

    Mesh* mesh = this->app->mesh;

    if(this->fullWrite){
        std::ofstream state(this->dir + "/state/" + std::to_string(this->index));
        state << std::setprecision(18);
        for(unsigned int i = 0; i < mesh->num_volumes; i++){
            this->write_vector(state,mesh->volumes[i]->state,this->app->nv,&(mesh->volumes[i]->stateIsActive));
            state << '\n';
        }
        state.close();

        std::ofstream flux(this->dir + "/flux/" + std::to_string(this->index));
        flux << std::setprecision(18);
        for(unsigned int i = 0; i < mesh->num_faces; i++){
            this->write_vector(flux,mesh->faces[i]->flux,this->app->nv);
            flux << '\n';
        }
        flux.close();
    }

    Volume* fluxGhostNode = mesh->boundaries[FACETYPE_BOUNDARY_LEFT].front();
    Face* fluxFace = (fluxGhostNode->nvol[0]->face[1]->type != FACETYPE_INTERNAL) ? fluxGhostNode->nvol[0]->face[1] : fluxGhostNode->nvol[0]->face[0];
    Volume* innerFluxNode = (fluxFace->nvol[0] == fluxGhostNode) ? fluxFace->nvol[1] : fluxFace->nvol[0];
    fluxFace = (innerFluxNode->face[0] == fluxFace) ? innerFluxNode->face[1] : innerFluxNode->face[0];

    // Calculate the displacement current  
    double E = fluxFace->getField();
    double displacementCurrent = 0;
    //if (indexval > 0) displacementCurrent = 1/this->app->p->debye * (E-this->lastE)/(indexval - this->lastTime);

    this->lastTime = indexval;
    this->lastE = E;

    Zone *cpleft_zone = app->mesh->getZone("CPLEFT");
    double ptotal = 0;
    for (auto & v : cpleft_zone->volumes)
        ptotal += v->state[0] * v->volume;
    //TODO: this is not general

    if(this->index > 0) this->json << ",\n";
    this->json << '"' << this->index << "\":{\"time\":" << indexval << ",\"voltage\":";
    this->json << this->app->getOCP(this->app->nv-2);
    this->json << ",\"ptotal\":" << ptotal;
    this->json << ",\"pulse\":" << app->pulse;
    this->json << ",\"mode\":\"" << app->mode << "\"";
    this->json << ",\"displacementCurrent\":" << displacementCurrent << ",\"fluxes\":[";
    this->write_vector(this->json,fluxFace->flux,this->app->nv);
    this->json << "]}";

    this->json.flush();

    this->index++;


    double rhototal = 0;
    for(unsigned int vi = 0; vi < app->mesh->num_volumes; vi++){
        Volume* v = app->mesh->volumes[vi];
        for(unsigned int si = 0; si < v->num_states-2 ; si++){
            if(v->stateIsActive[si])
                rhototal += v->state[si] * app->p->z[si] * v->volume;
        }
        rhototal += v->charge * v->volume;
    }
    std::cout << std::setprecision(20) << "Rho total: \033[1;31m " << rhototal << "\033[0m\n";
    
}

void ResultWriter::write_volumes(){
    if(this->MPIrank() != 0) return;

    std::ofstream fs(this->dir + "/volumes");
    fs << std::setprecision(18);

    for(unsigned int i = 0; i < this->app->mesh->num_volumes; i++) {
        Node& c = this->app->mesh->volumes[i]->center;
        fs << c.x << ',' << c.y << ',' << c.z << '\n';
    }

    fs.close();
}

void ResultWriter::write_faces(){
    if(this->MPIrank() != 0) return;

    std::ofstream fs(this->dir + "/faces");
    fs << std::setprecision(18);

    for(unsigned int i = 0; i < this->app->mesh->num_faces; i++) {
        Node& c = this->app->mesh->faces[i]->center;
        fs << c.x << ',' << c.y << ',' << c.z << '\n';
    }

    fs.close();
}

void ResultWriter::write_vector(std::ofstream& fs, double *vec, size_t len, std::vector<bool>* stateIsActive){
    if(this->MPIrank() != 0) return;

    for(unsigned int i = 0; i < len; i++) {
        // if(!stateIsActive || (*stateIsActive)[i]) 
            fs << vec[i];
        if(i < len-1) fs << ',';
    }
}