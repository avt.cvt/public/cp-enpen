#ifndef PARAMETERS_H
#define PARAMETERS_H
#include <vector>
#include <map>
#include <memory>
#include <iostream>
#include <fstream>
#include <cmath>

class Reaction {

public:
    std::vector<int> mu;
    std::vector<int> nu;
    double k_f, k_b;

    Reaction(int num_species) : mu(num_species), nu(num_species){}
};

class ParameterStore{

public:
    std::map<std::string, int> nameMap;
    unsigned int num_species;           /* Number of species */
    unsigned int num_reactions;         /* Number of reactions */
    unsigned int num_groups;            /* Number of functional groups */
    int *z;                             /* valence of species */
    int *group_valence;                 /* valence of functional group */
    double *mw;                         /* molecular weight */
    double *D;                          /* diffusion coeff. */
    double *hydration;                  /* hydration number */
    int **mu;                           /* forward reaction coeff. */
    int **nu;                           /* backward reaction coeff. */
    int **group_nu;                     /* group coeff. */
    double *k_f, *k_b;                  /* reaction speeds */
    double T, eta, rho;                 /* temperature, viscosity, density */
    double closest_approach;            /* PDH closest approach parameter */
    double F, R;                        /* Faraday const., id. gas const. */
    double k_boltzmann;
    double electron_charge;
    double epsilon;                     /* medium permittivity */
    char **name;                        /* species names */
    bool isNormalized = false;
    double c0;                          /* concentration normalization */
    double f;
    double D0;                          /* diffusion coeff. normalization */
    double mu0;                         /* viscosity normalization */
    double L0;                          /* length normalization */
    double p0;                          /* pressure normalization */
    double u0;                          /* velocity normalization */
    double t0;                          /* time normalization */
    double phi0;                        /* potential normalization */
    double debye;                       /* system debye number */
    double kappa;                       /* membrane permeability */
    const char *filename;                     /* system definition filename */
    double u;                           /* convective velocity, dimensionless */
    struct param_impedance_s {
        double f;
        double bias;
        double modulus;
    } impedance;
    std::map<std::string, int> species;
    bool inert = false;
    double C_v; // volumetric capacitance
    double B; 

    ParameterStore(std::string file);

    void setNorm(double L0 = 1e-6, double c0 = 1000, double D0 =1.0e-9);
    void load(std::string file);
    double DebyeLength(double *c);
    void setB(double B = -0.77);
    double normalizeBMod(double BMod);
    ~ParameterStore();
};

#endif
