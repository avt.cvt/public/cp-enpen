#include "ParameterStore.h"

ParameterStore::ParameterStore(std::string file){
    this->load(file);

    this->k_boltzmann = 1.38064852e-23;
    this->electron_charge = 1.60217662e-19;
    this->C_v = 204; // mol/m^3/V -> normalize this
    this->B = -0.767; // V -> normalize

    for(unsigned int i = 0; i<this->num_species; i++)
        this->nameMap[this->name[i]] = i;

    this->nameMap["PHI_CP"] = this->num_species;
    this->nameMap["PHI"] = this->num_species+1;
}

void ParameterStore::setB(double B){
    this->B = B / this->phi0;
}

double ParameterStore::normalizeBMod(double BMod){
    return BMod / this->phi0;
}

void ParameterStore::setNorm(double L0, double c0, double D0){
    if(this->isNormalized) throw std::runtime_error("Tried to normalize the parameters twice.");
    this->isNormalized = true;
    this->c0 = c0;
    this->D0 = D0;
    this->L0 = L0;

    this->debye = pow(this->F, 2.0) * this->c0 *
        pow(this->L0, 2.0) /
        (this->R * this->T * this->epsilon);

    this->mu0 = pow(this->L0, 2.0) * this->c0 *
                    this->R * this->T / this->D0;
    this->phi0 = this->R * this->T / this->F;
    this->p0 = this->c0 * this->R * this->T;
    this->u0 = this->D0/this->L0;
    this->t0 = pow(this->L0, 2.0)/this->D0;

    this->C_v *= this->phi0 / this->c0;
    this->B /= this->phi0;

    for(unsigned int i = 0; i < this->num_species; i++) {
        this->D[i] /= this->D0;
    }

    // units are expected in mol/l/s
    for(unsigned int i = 0; i < this->num_reactions; i++) {
        this->k_f[i] *= this->t0;
        this->k_b[i] *= this->t0;
    }
}

void ParameterStore::load(std::string file){

    std::ifstream pfile(file);

    pfile >> this->num_species >> this->num_reactions >> this->num_groups;

    size_t N = this->num_species;
    size_t G = this->num_groups;
    size_t R = this->num_reactions;
    this->z = new int[N];
    this->mw = new double[N];
    this->D = new double[N];
    this->hydration = new double[N];
    this->group_valence = new int[G];
    this->nu = (int**)malloc(N * sizeof(int *));
    this->mu = (int**)malloc(N * sizeof(int *));
    this->group_nu = (int**)malloc(N * sizeof(int *));
    this->name = (char**)malloc(N * sizeof(char *));
    this->k_f = (double*)malloc(R * sizeof(double));
    this->k_b = (double*)malloc(R * sizeof(double));
    for(unsigned int i = 0; i < N; i++) {
        this->nu[i] = (int*)malloc(R * sizeof(int));
        this->mu[i] = (int*)malloc(R * sizeof(int));
        this->group_nu[i] = (int*)malloc(G * sizeof(int));
        this->name[i] = (char*)malloc(256);
    }

    /* Parse species */
    for(unsigned int i = 0; i < N; i++) {
        pfile >> this->name[i] >> this->z[i] >> this->mw[i] >> this->D[i] >> this->hydration[i];
        this->species.insert(std::pair<std::string, int>(this->name[i], i));
    }

    /* Parse reactions */
    for(unsigned int i = 0; i < R; i++) {
        for(unsigned int j = 0; j < N; j++) {
            int coeff;

            pfile >> coeff;
            this->mu[j][i]=coeff>0?coeff:0; // forward
            this->nu[j][i]=coeff<0?coeff:0; // backward
        }

        pfile >> this->k_f[i] >> this->k_b[i];
    }

    /* Parse group valences */
    for(unsigned int i = 0; i < G; i++) pfile >> this->group_valence[i];

    /* Parse group definitions */
    for(unsigned int i = 0; i < N; i++)
        for(unsigned int j = 0; j < G; j++) 
            pfile >> this->group_nu[i][j];

    pfile >> this->T >> this->eta >> this->rho >> this->closest_approach >> this->F >> this->R >> this->epsilon;

    this->f = this->F/this->R/this->T;

    pfile.close();
    this->phi0 = this->R * this->T / this->F;
}

ParameterStore::~ParameterStore(){
    unsigned int i;
    size_t N = this->num_species;
    if(this->z) delete[] this->z;
    if(this->mw) delete[] this->mw;
    if(this->D) delete[] (this->D);
    if(this->hydration) delete[] (this->hydration);
    if(this->group_valence) free(this->group_valence);
    if(this->k_f) free(this->k_f);
    if(this->k_b) free(this->k_b);
    for(i = 0; i < N; i++) {
        if(this->nu&&this->nu[i]) free(this->nu[i]);
        if(this->mu&&this->mu[i]) free(this->mu[i]);
        if(this->group_nu&&this->group_nu[i]) free(this->group_nu[i]);
        if(this->name&&this->name[i]) free(this->name[i]);
    }
    if(this->nu) free(this->nu);
    if(this->mu) free(this->mu);
    if(this->group_nu) free(this->group_nu);
    if(this->name) free(this->name);
}

double ParameterStore::DebyeLength(double *c) {
    
    double Ix = 0.0;
    for(unsigned int i = 0; i < this->num_species; i++) {
        Ix += 0.5*pow(this->z[i],2)*c[i]; 
    }
    return sqrt(this->epsilon*this->R*this->T/pow(this->F, 2.0)/Ix);
}
