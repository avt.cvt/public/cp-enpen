#include "Application.h"


PetscErrorCode ts_pre_step(TS ts);
PetscErrorCode ts_post_step(TS ts);
PetscErrorCode ts_pre_step_cv(TS ts);
PetscErrorCode ts_pre_step_eis(TS ts);
PetscErrorCode tsmon(TS ts, PetscInt steps, PetscReal time, Vec u, void *context);

// Constructor, initialisiert Datenstruktur
Application::Application(ParameterStore *p, Model *model){
    this->p = p;
    this->solverdata = new SolverData();
    this->model = model;
    this->rw = NULL;

    this->nv = this->p->num_species + this->model->num_add_vars;
    this->model->bs = this->nv;
    this->mode = "write";
}


// Übergibt das Mesh an App und Konfiguriert die Anzahl der Variablen überall.
void Application::setMesh(Mesh* mesh){
    if(mesh->isInitialized()) {
        this->mesh = mesh;
        this->solverdata->init(this->nv,mesh->numVariables(),this);
        this->p->setNorm(this->mesh->mesh_unit);
    } else throw std::runtime_error("Mesh has to be initialized.");

    // fixes a strange convergence bug
    PetscErrorCode ierr;
    SolverData *sd = (SolverData *) this->solverdata;
    ierr = TSSetType(sd->ts,TSPSEUDO); CHKERRXX(ierr);
}

// Löscht PETSc Objekte
void Application::destroy(){
    this->solverdata->destroy();
}

// Destructor, räumt auf.
Application::~Application(){
    delete this->solverdata;
}


// Wird nach jedem Zeitschritt ausgeführt.
PetscErrorCode ts_post_step(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    // Holt aktuelle Zeit
    ierr = TSGetTime(ts, &time); CHKERRXX(ierr);
    // Holt Referenz auf app
    ierr = TSGetApplicationContext(ts, &app); CHKERRXX(ierr);

    Model* model = (Model*) app->model;

    // Überträgt Lösungsvektor zurück ins Mesh mit aktuellen Lösung
    app->solverdata->copySolutionToMesh(app->mesh);

    // Berechen die Flüsse über alle Faces.
    for(unsigned int i = 0; i < app->mesh->num_faces; i++) {
        model->fnc_flux(app->mesh->faces[i], app);
    }
    // for (auto const& b : app->mesh->boundary_faces) {
    //     for(auto const& f: b)
    //         ierr = model->fnc_flux(f, app); CHKERRXX(ierr);
    // }

    // Ausgabe der aktuellen Lösung
    if(app->rw) app->rw->write_state(time*app->p->t0);

    TSType t;
    ierr = TSGetType(ts, &t);

    PetscPrintf(PETSC_COMM_WORLD, "%gs (%s)\n", time*app->p->t0,t);

    return 0;
}

// Simuliert schnell in den stady state
void Application::pseudoTransient(double final_time){
    if(!this->mesh) throw std::runtime_error("mesh not set");
    PetscPrintf(PETSC_COMM_WORLD, "Starting Pseudo Transient...\n");

    PetscErrorCode ierr;

    // Hole Pointer auf den solver. Kürzer als immer this->solverdata zu schreiben. 
    SolverData *sd = (SolverData *) this->solverdata;

    // Überträgt Zustände aus dem Mesh in den Lösungsvektor.
    sd->copyValuesFromMesh(this->mesh);

    // Verwende den Lösungsvekotr x, der im Solver definiert wird.
    ierr = TSSetSolution(sd->ts, sd->x); CHKERRXX(ierr);

    // Verwende den Pseudo Transienten Löser.
    TSSetType(sd->ts,TSPSEUDO);
    // Übernehme Kommandozeilenoptionen. Normalerweise keine gesetzt.
    ierr = TSSetFromOptions(sd->ts); CHKERRXX(ierr);

    // Monitor ist eine Alternative zu pre/post step methoden. Wird nicht verwendet -> cancel
    ierr = TSMonitorCancel(sd->ts); CHKERRXX(ierr);

    // Rufe nach jedem Zeitschritt ts_post_step auf
    ierr = TSSetPostStep(sd->ts, ts_post_step); CHKERRXX(ierr); 
    // Application mit sd->ts verknüpfen um überall auf app zugreifen zu können.
    ierr = TSSetApplicationContext(sd->ts, (void *) this); CHKERRXX(ierr);

    // Anfangszeitschritt wird gesetzt.
    double dt = 1e-18/this->p->t0;
    ierr = TSSetTimeStep(sd->ts, dt); CHKERRXX(ierr);

    // Die Simulationszeit wird gesetzt.
    final_time /= this->p->t0; /* In seconds */
    ierr = TSSetMaxTime(sd->ts, final_time); CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, PETSC_MAX_INT); CHKERRXX(ierr);

    // hier egal
    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_STEPOVER); CHKERRXX(ierr);

    // Alles fertig. Bereite Simulation vor.
    ierr = TSSetUp(sd->ts); CHKERRXX(ierr);

    // und go
    ierr = TSSolve(sd->ts, sd->x); CHKERRXX(ierr);

    // calc fluxes
    for(unsigned int i = 0; i < this->mesh->num_faces; i++) {
        model->fnc_flux(this->mesh->faces[i], this);
    }

    sd->copySolutionToMesh(this->mesh);
    if(this->rw) this->rw->write_state(0);

    // Setze Zeit zurück auf 0.
    ierr = TSSetTime(sd->ts, 0); CHKERRXX(ierr);
}


void Application::transient(double duration,double dt)
{
    if(!this->mesh) throw std::runtime_error("mesh not set");
    PetscPrintf(PETSC_COMM_WORLD, "Starting BEuler...\n");
    PetscErrorCode ierr;

    SolverData *sd = (SolverData *) this->solverdata;
    sd->copyValuesFromMesh(this->mesh);

    // Wird hier tatsächlich eingehalten
    dt = dt/this->p->t0; // in seconds

    // Statt transient verwenden wir backwards euler (impliziter euler)
    ierr = TSSetType(sd->ts,TSBEULER); CHKERRXX(ierr);

    // Starte die Simulation bei aktueller Zeit.
    PetscReal time;
    ierr = TSGetTime(sd->ts, &time); CHKERRXX(ierr);
    ierr = TSSetTimeStep(sd->ts, dt); CHKERRXX(ierr);
    ierr = TSSetSolution(sd->ts, sd->x); CHKERRXX(ierr);

    if(this->rw) this->rw->write_state(time*this->p->t0);

    double final_time = duration/this->p->t0 + time; /* In seconds */
    ierr = TSSetMaxTime(sd->ts, final_time); CHKERRXX(ierr);
    PetscInt current_step;
    ierr = TSGetStepNumber(sd->ts, &current_step); CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, current_step+final_time/dt); CHKERRXX(ierr);

    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_STEPOVER); CHKERRXX(ierr);

    ierr = TSMonitorCancel(sd->ts); CHKERRXX(ierr);
    // ierr = TSMonitorSet(sd->ts, tsmon, (void *) this, NULL); CHKERRXX(ierr);
    ierr = TSSetPostStep(sd->ts, ts_post_step); CHKERRXX(ierr); // Try PostStep instead of Monitor
    ierr = TSSetApplicationContext(sd->ts, (void *) this); CHKERRXX(ierr);

    // Rufe diese Funktion vor jedem Zeitschritt auf. Setzt zb. Potential oä.
    ierr = TSSetPreStep(sd->ts, ts_pre_step); CHKERRXX(ierr);

    ierr = TSSetUp(sd->ts); CHKERRXX(ierr);
    ierr = TSSolve(sd->ts, sd->x); CHKERRXX(ierr);
    sd->copySolutionToMesh(this->mesh);
}


// Wird vor jedem Zeitschritt aufgerufen und gibt ein Sägezahn Spannungsprofil vor.
PetscErrorCode ts_pre_step_cv(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    ierr = TSGetTime(ts, &time); CHKERRXX(ierr);
    ierr = TSGetApplicationContext(ts, &app); CHKERRXX(ierr);

    // Berechne Sägezahnspannung
    PetscReal amplitude = app->CVAmplitude;
    PetscReal interval = app->CVAmplitude/app->CVScanRate*2 /app->p->t0;
    
    PetscReal interval_time = fmod(time/interval,1);

    PetscReal bias = 0;
    if (interval_time < 0.25) {
        bias = amplitude*interval_time*2;
    } else if (interval_time < 0.75) {
        bias = (-amplitude*(interval_time-0.25) + 0.25*amplitude)*2;
    } else {
        bias = (amplitude*(interval_time-0.75) - 0.25*amplitude)*2;
    }

    // boundary contraints are set to the calculated values
    app->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, app->p->num_species); 
    app->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, app->p->num_species+1); 
    PetscPrintf(PETSC_COMM_WORLD, "%gs (pre step): %g\n", time*app->p->t0,bias); 
    return 0;
}

void Application::adaptiveTransient(double duration,double dt)
{
    if(!this->mesh) throw std::runtime_error("mesh not set");
    PetscPrintf(PETSC_COMM_WORLD, "Starting BEuler...\n");
    PetscErrorCode ierr;

    SolverData *sd = (SolverData *) this->solverdata;
    sd->copyValuesFromMesh(this->mesh);

    // Wird hier tatsächlich eingehalten
    dt = dt/this->p->t0; // in seconds

    // Statt transient verwenden wir backwards euler (impliziter euler)
    ierr = TSSetType(sd->ts,TSBEULER); CHKERRXX(ierr);
    TSAdapt adapt;
    ierr = TSGetAdapt(sd->ts, &adapt); CHKERRXX(ierr);
    ierr = TSAdaptSetType(adapt, TSADAPTBASIC); CHKERRXX(ierr);
 
    // Starte die Simulation bei aktueller Zeit.
    PetscReal time;
    ierr = TSGetTime(sd->ts, &time); CHKERRXX(ierr);
    ierr = TSSetTimeStep(sd->ts, dt); CHKERRXX(ierr);
    ierr = TSSetSolution(sd->ts, sd->x); CHKERRXX(ierr);

    double final_time = duration/this->p->t0 + time; /* In seconds */
    ierr = TSSetMaxTime(sd->ts, final_time); CHKERRXX(ierr);
    PetscInt current_step;
    ierr = TSGetStepNumber(sd->ts, &current_step); CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, current_step+final_time/dt); CHKERRXX(ierr);

    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_STEPOVER); CHKERRXX(ierr);

    ierr = TSMonitorCancel(sd->ts); CHKERRXX(ierr);
    // ierr = TSMonitorSet(sd->ts, tsmon, (void *) this, NULL); CHKERRXX(ierr);
    ierr = TSSetPostStep(sd->ts, ts_post_step); CHKERRXX(ierr); // Try PostStep instead of Monitor
    ierr = TSSetApplicationContext(sd->ts, (void *) this); CHKERRXX(ierr);

    // Rufe diese Funktion vor jedem Zeitschritt auf. Setzt zb. Potential oä.
    ierr = TSSetPreStep(sd->ts, ts_pre_step); CHKERRXX(ierr);

    ierr = TSSetUp(sd->ts); CHKERRXX(ierr);
    ierr = TSSolve(sd->ts, sd->x); CHKERRXX(ierr);
    sd->copySolutionToMesh(this->mesh);
}

// Startet cv Simulation
void Application::cyclicVoltammetry(double scanRate, double amplitude){
    this->CVScanRate = scanRate;
    this->CVAmplitude = amplitude;

    double cycles = 2;
    int steps = 1e2;
    double dtmin = 1e-1 / this->p->t0;

    if(!this->mesh) throw std::runtime_error("mesh not set");
    PetscPrintf(PETSC_COMM_WORLD, "Starting cyclic voltammetry (BEuler)...\n");
    PetscErrorCode ierr;

    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, 0, this->p->num_species);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_RIGHT, 0, this->p->num_species);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, 0, this->p->num_species+1);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_RIGHT, 0, this->p->num_species+1);

    SolverData *sd = (SolverData *) this->solverdata;
    sd->copyValuesFromMesh(this->mesh);

    double duration = amplitude/scanRate*(2*cycles) /this->p->t0;
    double dt = duration/steps/(2*cycles);
    dt = std::min(dt,dtmin);

    ierr = TSSetType(sd->ts,TSBEULER); CHKERRXX(ierr);
    PetscReal time;
    ierr = TSGetTime(sd->ts, &time); CHKERRXX(ierr);
    ierr = TSSetTimeStep(sd->ts, dt); CHKERRXX(ierr);
    ierr = TSSetSolution(sd->ts, sd->x); CHKERRXX(ierr);

    double final_time = duration + time;
    ierr = TSSetMaxTime(sd->ts, final_time); CHKERRXX(ierr);
    PetscInt current_step;
    ierr = TSGetStepNumber(sd->ts, &current_step); CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, current_step+final_time/dt); CHKERRXX(ierr);

    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_STEPOVER); CHKERRXX(ierr);

    ierr = TSMonitorCancel(sd->ts); CHKERRXX(ierr);
    // ierr = TSMonitorSet(sd->ts, tsmon, (void *) this, NULL); CHKERRXX(ierr);
    ierr = TSSetPostStep(sd->ts, ts_post_step); CHKERRXX(ierr); // Try PostStep instead of Monitor
    ierr = TSSetApplicationContext(sd->ts, (void *) this); CHKERRXX(ierr);

    ierr = TSSetPreStep(sd->ts, ts_pre_step_cv); CHKERRXX(ierr);

    ierr = TSSetUp(sd->ts); CHKERRXX(ierr);
    ierr = TSSolve(sd->ts, sd->x); CHKERRXX(ierr);
    sd->copySolutionToMesh(this->mesh);
}


// Tut quasi nix..
PetscErrorCode ts_pre_step(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    ierr = TSGetTime(ts, &time); CHKERRXX(ierr);
    ierr = TSGetApplicationContext(ts, &app); CHKERRXX(ierr);

    PetscPrintf(PETSC_COMM_WORLD, "%gs (pre step)\n", time*app->p->t0);

    return 0;
}


// Vermutlich nicht mehr verwendet. Siehe Post Step
PetscErrorCode tsmon(TS ts, PetscInt steps, PetscReal time, Vec u, void *context)
{
    Application *app = (Application *) context;
    Model* model = (Model*) app->model;
    app->solverdata->copySolutionToMesh(app->mesh);
    unsigned int i;
    for(i = 0; i < app->mesh->num_faces; i++) {
        model->fnc_flux(app->mesh->faces[i], app);
    }
    if(app->rw) app->rw->write_state(time*app->p->t0);

    TSType t;
    PetscErrorCode ierr = TSGetType(ts, &t); CHKERRXX(ierr);

    PetscPrintf(PETSC_COMM_WORLD, "%gs (%s)\n", time*app->p->t0,t);

    return 0;
}

// Gibt eine sinusförmige Spannung vor
PetscErrorCode ts_pre_step_eis(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    ierr = TSGetTime(ts, &time); CHKERRXX(ierr);
    ierr = TSGetApplicationContext(ts, &app); CHKERRXX(ierr);

    constexpr double pi = 3.14159265358979323846;

    PetscReal bias = app->EISAmplitude*sin(2*pi*app->EISFrequency*(time - app->EISt0)*app->p->t0) + app->EISBias;
    
    // boundary contraints are set to the calculated values
    app->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, app->p->num_species); 
    app->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, app->p->num_species+1); 
    PetscPrintf(PETSC_COMM_WORLD, "%gs (pre step): %g\n", time*app->p->t0,bias); 
    return 0;
}

void Application::impedance(double frequency, double amplitude, double bias){
    this->EISFrequency = frequency;
    this->EISAmplitude = amplitude;
    this->EISBias = bias;

    double cycles = 2;
    int steps = 1e2;
    double dtmin = 1e-1 / this->p->t0;

    if(!this->mesh) throw std::runtime_error("mesh not set");
    PetscPrintf(PETSC_COMM_WORLD, "Starting impedance (BEuler)...\n");
    PetscErrorCode ierr;

    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, 0, this->p->num_species);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_RIGHT, 0, this->p->num_species);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, 0, this->p->num_species+1);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_RIGHT, 0, this->p->num_species+1);

    SolverData *sd = (SolverData *) this->solverdata;
    sd->copyValuesFromMesh(this->mesh);

    double duration = 1/frequency*cycles /this->p->t0;
    double dt = duration/steps/cycles;
    dt = std::min(dt,dtmin);

    ierr = TSSetType(sd->ts,TSBEULER); CHKERRXX(ierr);
    PetscReal time;
    ierr = TSGetTime(sd->ts, &time); CHKERRXX(ierr);
    ierr = TSSetTimeStep(sd->ts, dt); CHKERRXX(ierr);
    ierr = TSSetSolution(sd->ts, sd->x); CHKERRXX(ierr);

    this->EISt0 = time;

    double final_time = duration + time;
    ierr = TSSetMaxTime(sd->ts, final_time); CHKERRXX(ierr);
    PetscInt current_step;
    ierr = TSGetStepNumber(sd->ts, &current_step); CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, current_step+final_time/dt); CHKERRXX(ierr);
    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_STEPOVER); CHKERRXX(ierr);

    ierr = TSMonitorCancel(sd->ts); CHKERRXX(ierr);
    // ierr = TSMonitorSet(sd->ts, tsmon, (void *) this, NULL); CHKERRXX(ierr);
    ierr = TSSetPostStep(sd->ts, ts_post_step); CHKERRXX(ierr); // Try PostStep instead of Monitor
    ierr = TSSetApplicationContext(sd->ts, (void *) this); CHKERRXX(ierr);

    ierr = TSSetPreStep(sd->ts, ts_pre_step_eis); CHKERRXX(ierr);

    ierr = TSSetUp(sd->ts); CHKERRXX(ierr);
    ierr = TSSolve(sd->ts, sd->x); CHKERRXX(ierr);
    sd->copySolutionToMesh(this->mesh);
}


PetscErrorCode ts_post_step_resistance(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    ierr = TSGetTime(ts, &time); CHKERRXX(ierr);
    ierr = TSGetApplicationContext(ts, &app); CHKERRXX(ierr);

    Model* model = (Model*) app->model;
    Mesh* mesh = (Mesh*) app->mesh;
    ParameterStore* p = (ParameterStore*) app->p;


    app->solverdata->copySolutionToMesh(app->mesh);
    unsigned int i;
    for(i = 0; i < app->mesh->num_faces; i++) {
        model->fnc_flux(app->mesh->faces[i], app);
    }
  
    if(app->rw) app->rw->write_state(time*app->p->t0);

    // get flux, calculate voltage drop and set new boundary condition

    Volume* fluxGhostNode = mesh->boundaries[FACETYPE_BOUNDARY_RIGHT].front();
    Face* fluxFace = (fluxGhostNode->nvol[0]->face[1]->type != FACETYPE_INTERNAL) ? fluxGhostNode->nvol[0]->face[1] : fluxGhostNode->nvol[0]->face[0];
    app->current = fluxFace->flux[0];//*-1;
    app->deviceVoltage = app->appliedVoltage - app->current * app->droppingResistance;

    mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, app->deviceVoltage, p->num_species);

    TSType t;
    ierr = TSGetType(ts, &t);

    PetscPrintf(PETSC_COMM_WORLD, "%gs (%s)\n", time*app->p->t0,t);

    return 0;
}


void Application::transientWithResistance(double duration,double dt, double R, double V)
{
    if(!this->mesh) throw std::runtime_error("mesh not set");
    this->droppingResistance = R*40*1e-4*1.602176634e-19*6.02214076e23 ; //Von [Ohm] in [tV*s*m²*mol⁻¹] umrechnen (Avogadroconst, Elementarladung, Fläche:1cm², 40tV)
    this->appliedVoltage = V;//this->mesh->boundaries[FACETYPE_BOUNDARY_LEFT].front()->state[this->p->num_species];

    PetscPrintf(PETSC_COMM_WORLD, "Starting BEuler...\n");
    PetscErrorCode ierr;

    SolverData *sd = (SolverData *) this->solverdata;
    sd->copyValuesFromMesh(this->mesh);

    dt = dt/this->p->t0; // in seconds
    ierr = TSSetType(sd->ts,TSBEULER); CHKERRXX(ierr);
    PetscReal time;
    ierr = TSGetTime(sd->ts, &time); CHKERRXX(ierr);
    ierr = TSSetTimeStep(sd->ts, dt); CHKERRXX(ierr);
    ierr = TSSetSolution(sd->ts, sd->x); CHKERRXX(ierr);

    double final_time = duration/this->p->t0 + time; /* In seconds */
    ierr = TSSetMaxTime(sd->ts, final_time); CHKERRXX(ierr);
    PetscInt current_step;
    ierr = TSGetStepNumber(sd->ts, &current_step); CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, current_step+final_time/dt); CHKERRXX(ierr);

    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_STEPOVER); CHKERRXX(ierr);

    ierr = TSMonitorCancel(sd->ts); CHKERRXX(ierr);
    ierr = TSSetPostStep(sd->ts, ts_post_step_resistance); CHKERRXX(ierr); 
    ierr = TSSetApplicationContext(sd->ts, (void *) this); CHKERRXX(ierr);

    ierr = TSSetPreStep(sd->ts, ts_pre_step); CHKERRXX(ierr);

    ierr = TSSetUp(sd->ts); CHKERRXX(ierr);
    ierr = TSSolve(sd->ts, sd->x); CHKERRXX(ierr);
    sd->copySolutionToMesh(this->mesh);
}

// Wird vor jedem Zeitschritt aufgerufen und gibt ein Sägezahn Spannungsprofil vor.
PetscErrorCode ts_pre_step_cv_WR(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    ierr = TSGetTime(ts, &time); CHKERRXX(ierr);
    ierr = TSGetApplicationContext(ts, &app); CHKERRXX(ierr);

    Mesh* mesh = (Mesh*) app->mesh;
    ParameterStore* p = (ParameterStore*) app->p;

    // Berechne Sägezahnspannung
    PetscReal amplitude = app->CVAmplitude;
    PetscReal interval = app->CVAmplitude/app->CVScanRate*2 /app->p->t0;
    
    PetscReal interval_time = fmod(time/interval,1);

    PetscReal bias = 0;
    if (interval_time < 0.25) {
        bias = amplitude*interval_time*2;
    } else if (interval_time < 0.75) {
        bias = (-amplitude*(interval_time-0.25) + 0.25*amplitude)*2;
    } else {
        bias = (amplitude*(interval_time-0.75) - 0.25*amplitude)*2;
    }
    app->appliedVoltage = bias;
    
    // get flux, calculate voltage drop and set new boundary condition

    Volume* fluxGhostNode = mesh->boundaries[FACETYPE_BOUNDARY_RIGHT].front();
    Face* fluxFace = (fluxGhostNode->nvol[0]->face[1]->type != FACETYPE_INTERNAL) ? fluxGhostNode->nvol[0]->face[1] : fluxGhostNode->nvol[0]->face[0];
    app->current = fluxFace->flux[0];//*-1;
    app->deviceVoltage = app->appliedVoltage - app->current * app->droppingResistance;
    mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, app->deviceVoltage, p->num_species);
    PetscPrintf(PETSC_COMM_WORLD, "%gs (pre step): %g\n", time*app->p->t0,bias); 
    return 0;
}

// Wird nach jedem Zeitschritt ausgeführt.
PetscErrorCode ts_post_step_WR(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    // Holt aktuelle Zeit
    ierr = TSGetTime(ts, &time); CHKERRXX(ierr);
    // Holt Referenz auf app
    ierr = TSGetApplicationContext(ts, &app); CHKERRXX(ierr);

    Model* model = (Model*) app->model;
    ParameterStore* p = (ParameterStore*) app->p;
    Mesh* mesh = (Mesh*) app->mesh;


    // Überträgt Lösungsvektor zurück ins Mesh mit aktuellen Lösung
    app->solverdata->copySolutionToMesh(app->mesh);

    // Berechen die Flüsse über alle Faces.
    for(unsigned int i = 0; i < app->mesh->num_faces; i++) {
        model->fnc_flux(app->mesh->faces[i], app);
    }
    // for (auto const& b : app->mesh->boundary_faces) {
    //     for(auto const& f: b)
    //         ierr = model->fnc_flux(f, app); CHKERRXX(ierr);
    // }

    // Ausgabe der aktuellen Lösung
    mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, app->appliedVoltage, p->num_species);
    if(app->rw) app->rw->write_state(time*app->p->t0);

    TSType t;
    ierr = TSGetType(ts, &t);

    PetscPrintf(PETSC_COMM_WORLD, "%gs (%s)\n", time*app->p->t0,t);

    return 0;
}

void Application::cyclicVoltammetry_WR(double scanRate, double amplitude, double R){
    this->CVScanRate = scanRate;
    this->CVAmplitude = amplitude;
    this->droppingResistance = R*40*1e-4*1.602176634e-19*6.02214076e23 ; //Von [Ohm] in [tV*s*m²*mol⁻¹] umrechnen (Avogadroconst, Elementarladung, Fläche:1cm², 40tV)
    this->appliedVoltage = 0;

    double cycles = 2;
    int steps = 1e1;
    double dtmin = 1e-1 / this->p->t0;

    if(!this->mesh) throw std::runtime_error("mesh not set");
    PetscPrintf(PETSC_COMM_WORLD, "Starting cyclic voltammetry (BEuler)...\n");
    PetscErrorCode ierr;

    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, 0, this->p->num_species);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_RIGHT, 0, this->p->num_species);

    SolverData *sd = (SolverData *) this->solverdata;
    sd->copyValuesFromMesh(this->mesh);

    double duration = amplitude/scanRate*(2*cycles) /this->p->t0;
    double dt = duration/steps/(2*cycles);
    dt = std::min(dt,dtmin);

    ierr = TSSetType(sd->ts,TSBEULER); CHKERRXX(ierr);
    PetscReal time;
    ierr = TSGetTime(sd->ts, &time); CHKERRXX(ierr);
    ierr = TSSetTimeStep(sd->ts, dt); CHKERRXX(ierr);
    ierr = TSSetSolution(sd->ts, sd->x); CHKERRXX(ierr);

    double final_time = duration + time;
    ierr = TSSetMaxTime(sd->ts, final_time); CHKERRXX(ierr);
    PetscInt current_step;
    ierr = TSGetStepNumber(sd->ts, &current_step); CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, current_step+final_time/dt); CHKERRXX(ierr);

    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_STEPOVER); CHKERRXX(ierr);

    ierr = TSMonitorCancel(sd->ts); CHKERRXX(ierr);
    // ierr = TSMonitorSet(sd->ts, tsmon, (void *) this, NULL); CHKERRXX(ierr);
    ierr = TSSetPostStep(sd->ts, ts_post_step_WR); CHKERRXX(ierr); // Try PostStep instead of Monitor
    ierr = TSSetApplicationContext(sd->ts, (void *) this); CHKERRXX(ierr);

    ierr = TSSetPreStep(sd->ts, ts_pre_step_cv_WR); CHKERRXX(ierr);

    ierr = TSSetUp(sd->ts); CHKERRXX(ierr);
    ierr = TSSolve(sd->ts, sd->x); CHKERRXX(ierr);
    sd->copySolutionToMesh(this->mesh);
}


double Application::getOCP(int index){
    Volume* vl = this->mesh->boundaries[FACETYPE_BOUNDARY_LEFT].front()->nvol[0];
    Volume* vr = this->mesh->boundaries[FACETYPE_BOUNDARY_RIGHT].front()->nvol[0];
    return vl->state[index] + (this->p->B + vl->Bmod) - (vr->state[index] + (this->p->B + vr->Bmod));
}