#include "TybrandtShuttleNPP.h"
#include <cmath>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include "BVReaction.h"


void TybrandtShuttleNPP::fnc_time(Volume *v, PetscScalar *in, PetscScalar *din,
        PetscScalar *dst, unsigned int iVec, Application *ctx)
{
    unsigned int vi = v->solution_index;
    /* Store transient term for i in dst */
    *dst = v->volume * din[vi+iVec];
}

void TybrandtShuttleNPP::jac_time(Volume *v, PetscScalar *in,
        PetscScalar *din, PetscScalar *dst, unsigned int iVec, Application *ctx)
{
    /* Calculate dF/dU_t for i and store it at dst */
    *dst = v->volume;
}


void TybrandtShuttleNPP::fnc_spec(Volume *v, PetscScalar *in, PetscScalar *dst,
        unsigned int i, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    ParameterStore *p = ctx->p;

    if(!v->stateIsActive[i]) return;

    PetscScalar c = in[vi+i];
    PetscScalar phi = in[vi+nos-1];

    /* Flux term */
    PetscScalar flux = 0;
    for(unsigned int f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];
        Volume* nv = v->nvol[f];
        Face* face = v->face[f];

        PetscScalar c_n, phi_n;
        unsigned int ni = nv->solution_index;

        phi_n = in[ni+nos-1];
        c_n = in[ni+i];

        if(face->bcond_type[nos-1] == 0)
            phi_n = face->boundary_states[nos-1];
        if(face->bcond_type[i] == 0)
            c_n = face->boundary_states[i];

        PetscScalar c_f = 0.5 * (c + c_n);
        PetscScalar alpha = std::max(v->alpha,nv->alpha);

        if(face->bcond_type[i] == 2) // no flux boundary
            flux += 0; 
        else if(!nv->stateIsActive[i])
            flux += 0;
        else
            flux += A/ksi*alpha*p->D[i]*((c_n-c) + p->z[i] * c_f * (phi_n-phi));
    }

    PetscScalar source = 0;

    /* Electrochemical Reaction */
    if(v->bvreaction != NULL){
        BVReaction* er = v->bvreaction;
        double phi = in[vi+nos-1];
        double phi_cp = in[vi+nos-2];
        double eta = -1* ((phi_cp - phi + (p->B+v->Bmod)) - er->E0);
        double RcoeffBV = (er->mu[i] + er->nu[i]) * er->k_0 * v->volume;

        source = RcoeffBV * (in[vi + er->ox]/er->c_oe * exp(er->alpha*er->n*eta)
            - in[vi + er->red]/er->c_re*exp(-1*(1-er->alpha)*er->n*eta));
    }

    /* Mass Action Law Reaction */
    for(unsigned int j = 0; j < p->num_reactions; j++) {
        double RcoeffMA = (p->mu[i][j] + p->nu[i][j]) * v->volume;
        double p_f = 1.0, p_b = 1.0;
        for(unsigned int m = 0; m < p->num_species; m++) {
            p_f *= pow(in[vi+m], p->mu[m][j]);
            p_b *= pow(in[vi+m], -p->nu[m][j]);
        }
        source += RcoeffMA * (p->k_f[j] * p_f - p->k_b[j] * p_b);
    }

    *dst = 0 - flux + source;
}

void TybrandtShuttleNPP::jac_spec_local(Volume *v, PetscScalar *in,
        PetscScalar *dst, unsigned int i, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    ParameterStore *p = ctx->p;

    if(!v->stateIsActive[i]) return;

    PetscScalar c = in[vi+i];
    PetscScalar phi = in[vi+nos-1];

    PetscScalar dc = 0;
    PetscScalar dphi = 0;
    unsigned int f;
    for(f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Volume* nv = v->nvol[f];
        Face* face = v->face[f];
        unsigned int ni = nv->solution_index;

        PetscScalar c_n, phi_n, c_f;

        phi_n = in[ni+nos-1];
        c_n = in[ni+i];

        if(face->bcond_type[nos-1] == 0)
            phi_n = face->boundary_states[nos-1];
        if(face->bcond_type[i] == 0)
            c_n = face->boundary_states[i];

        c_f = 0.5 * (c + c_n);
        PetscScalar alpha = std::max(v->alpha,nv->alpha);

        PetscScalar factor = A*alpha*p->D[i]/ksi;

        if((face->bcond_type[i] == -1 || face->bcond_type[i] == 0) && nv->stateIsActive[i]){
            dc += factor * (1 - p->z[i] * 0.5 * (phi_n-phi));
            dphi += factor * p->z[i] * c_f;
        }
    }
    dst[i] = dc;
    dst[nos-1] = dphi;

    if(v->bvreaction != NULL){
        BVReaction* er = v->bvreaction;
        double phi = in[vi+nos-1];
        double phi_cp = in[vi+nos-2];
        double eta = -1*((phi_cp - phi + (p->B+v->Bmod)) - er->E0);

        double RcoeffBV = (er->mu[i] + er->nu[i]) * er->k_0 * v->volume;

        dst[er->ox] += RcoeffBV * 1/er->c_oe * exp(er->alpha*er->n*eta);
        dst[er->red] += RcoeffBV * -1/er->c_re * exp(-1*(1-er->alpha)*er->n*eta);

        double dRdphi = RcoeffBV * er->n * (er->alpha * in[vi + er->ox]/er->c_oe * exp(er->alpha*er->n*eta)
            + (1-er->alpha) * in[vi + er->red]/er->c_re * exp(-1*(1-er->alpha)*er->n*eta));

        dst[nos-1] += dRdphi;
        dst[nos-2] += -1*dRdphi;
    }

    /* Mass Action Reaction */
    for(unsigned int k = 0; k < p->num_species; k++) {
        double ds = 0;
        for(unsigned int j = 0; j < p->num_reactions; j++) {
            double RcoeffMA = (p->mu[i][j] + p->nu[i][j]) * v->volume;
            PetscScalar p_f = 1.0, p_b = 1.0;

            for(unsigned int m = 0; m < p->num_species; m++) {
                if(k == m) {
                    if(p->mu[m][j] > 1) p_f *= pow(in[vi+m], p->mu[m][j] -1);
                    if(-p->nu[m][j] > 1) p_b *= pow(in[vi+m], -p->nu[m][j] -1);
                } else {
                    p_f *= pow(in[vi+m], p->mu[m][j]);
                    p_b *= pow(in[vi+m], -p->nu[m][j]);
                }
            }
            ds += RcoeffMA * (p->k_f[j] * p->mu[k][j] * p_f - p->k_b[j] * (-p->nu[k][j]) * p_b);

        }
        dst[k] += ds;
    }

}

void TybrandtShuttleNPP::jac_spec_neighbour(Volume *v, PetscScalar *in,
        PetscScalar *dst, unsigned int i, unsigned int f, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    Volume *nv = v->nvol[f];
    ParameterStore *p = ctx->p;
    Face* face = v->face[f];

    if(!v->stateIsActive[i]) return;
    if(!nv->stateIsActive[i]) return;

    if(face->bcond_type[i] == -1){

        PetscScalar dphi = 0,dc = 0;
        unsigned int ni = nv->solution_index;

        PetscScalar c, phi, c_n, phi_n, c_f;
        phi = in[vi+nos-1];
        phi_n = in[ni+nos-1];

        c = in[vi+i];
        c_n = in[ni+i];
        c_f  = 0.5 * (c + c_n);
       
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];
        PetscScalar alpha = std::max(v->alpha,nv->alpha);

        PetscScalar factor = A*alpha*p->D[i]/ksi;

        dc = -factor * (1 + p->z[i] * 0.5 * (phi_n-phi));
        dst[i] = dc;

        dphi = -factor * p->z[i] * c_f;
        dst[nos-1] = dphi;
    }
}



void TybrandtShuttleNPP::fnc_pois(Volume *v, PetscScalar *in, PetscScalar *dst,
        Application *ctx)
{
    unsigned int vi = v->solution_index;
    unsigned int nos = v->num_states;
    ParameterStore *p = ctx->p;

    PetscScalar phi = in[vi+nos-1];

    /* Source term */
    PetscScalar rho = v->charge, source;
    for(unsigned int i = 0; i < nos-2; i++) {
        if(v->stateIsActive[i]) rho += in[vi+i] * p->z[i];
    }

    source = v->volume * rho * p->debye;

    /* Flux term */
    PetscScalar flux = 0;
    for(unsigned int f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Volume* nv = v->nvol[f];
        Face* face = v->face[f];
        PetscScalar phi_n;
        if(face->bcond_type[nos-1] == 0){
            phi_n = face->boundary_states[nos-1];
        } else {
            unsigned int ni = nv->solution_index;
            phi_n = in[ni+nos-1];
        }

        if(face->bcond_type[nos-1] == 2)
            flux += 0;
        else 
            flux += A / ksi * (phi_n - phi);
    }
    *dst = flux + source;
}


void TybrandtShuttleNPP::jac_pois_local(Volume *v, PetscScalar *in,
        PetscScalar *dst, Application *ctx)
{
    ParameterStore *p = ctx->p;
    unsigned int nos = v->num_states;

    PetscScalar dphi = 0;

    for(unsigned int f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Face* face = v->face[f];
        if(face->bcond_type[nos-1] == 2)
            dphi += 0;
        else
            dphi += -A/ksi;
    }
    dst[nos-1] = dphi;

    for(unsigned int i = 0; i < nos-2; i++) {
        if(v->stateIsActive[i]) dst[i] = v->volume * p->debye * p->z[i];
    }
}


void TybrandtShuttleNPP::jac_pois_neighbour(Volume *v, PetscScalar *in,
        PetscScalar *dst, unsigned int f, Application *ctx)
{
    Face* face = v->face[f];
    Volume* nv = v->nvol[f];
    unsigned int nos = nv->num_states;

    if(face->bcond_type[nos-1] == 2)
        dst[nos-1] += 0;
    else if(face->bcond_type[nos-1] == 0)
        dst[nos-1] += 0;
    else
        dst[nos-1] = v->face[f]->area/v->distance[f];
}


void TybrandtShuttleNPP::fnc_flux(Face *face, Application *ctx)
{
    Volume *v1, *v2;
    v1 = face->nvol[0];
    v2 = face->nvol[1];
    if(!v1||!v2) throw std::runtime_error("A face is not properly connected");

    ParameterStore *p = ctx->p;
    unsigned int nos = v1->num_states;

    // PetscScalar ksi = v2->center.x - v1->center.x;
    PetscScalar ksi = face->volumeDistance();
    ksi = (v1->center.x > v2->center.x) ? -1*ksi : ksi;
    PetscScalar dphi, dphi_cp, dc, c_f;
    dphi = (v2->state[nos-1] - v1->state[nos-1]);
    dphi_cp = (v2->state[nos-2] - v1->state[nos-2]);

    face->flux[nos-2] = 0;
    face->flux[nos-1] = 0;

    PetscScalar alpha = std::max(v1->alpha,v2->alpha);

    if(face->bcond_type[0] == 3){
        Volume* v = (v1->type == VOLTYPE_BULK) ? v2 : v1;
        Volume* nv = (v1->type == VOLTYPE_BULK) ? v1 : v2;
        double sign = (v1->type == VOLTYPE_BULK) ? 1 : -1;
        double phi_n_cp = nv->state[nos-2] - (p->B + v->Bmod);
        double phi = v->state[nos-1];
        double c_n = (phi_n_cp - phi) * p->C_v;
        c_f = 0.5 * (v->state[0] + c_n);
        face->flux[0] = sign/ksi*p->D[0]*((c_n-v->state[0]) + p->z[0] * c_f * (phi_n_cp-v->state[nos-2]));
        face->flux[0] = 0;
    } else if(face->bcond_type[0] == 2){
        face->flux[0] = 0;
    } else {
        dc = v2->state[0] - v1->state[0];
        c_f = 0.5*(v2->state[0] + v1->state[0]);
        face->flux[0] = -p->D[0]/ksi*(dc + p->z[0]*c_f*dphi_cp);
    }

    for(unsigned int i = 1; i < nos-2; i++) {
        dc = v2->state[i] - v1->state[i];
        c_f = 0.5*(v2->state[i] + v1->state[i]);

        if(!(v1->stateIsActive[i] && v2->stateIsActive[i]))
            face->flux[i] = 0;
        else if(face->bcond_type[i] > 1)
            face->flux[i] = 0;
        else
            face->flux[i] = -alpha*p->D[i]/ksi*(dc + p->z[i]*c_f*dphi);
    }
}



// Species CP

void TybrandtShuttleNPP::fnc_cp_spec(Volume *v, PetscScalar *in, PetscScalar *dst,
        unsigned int i, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    ParameterStore *p = ctx->p;

    if(!v->stateIsActive[i]) return;

    PetscScalar c = in[vi+i];
    PetscScalar phi_cp = in[vi+nos-2];

    /* Flux term */
    PetscScalar flux = 0;
    unsigned int f;
    for(f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];
        Volume* nv = v->nvol[f];
        Face* face = v->face[f];

        PetscScalar c_n, phi_n_cp;
        unsigned int ni = nv->solution_index;

        if(face->bcond_type[i] == 2) // no flux boundary
            flux += 0;
        else if(!nv->stateIsActive[i])
            flux += 0;
        else if(face->bcond_type[i] == 3){
            phi_n_cp = nv->state[nos-2] - (p->B + v->Bmod);
            PetscScalar phi = in[vi+nos-1];
            c_n = (phi_n_cp - phi) * p->C_v;
            PetscScalar c_f = 0.5 * (c + c_n);
            
            flux += A/ksi*p->D[i]*((c_n-c) + p->z[i] * c_f * (phi_n_cp-phi_cp));
        } else {
            c_n = in[ni+i];
            phi_n_cp = in[ni+nos-2];
            PetscScalar c_f = 0.5 * (c + c_n);
            flux += A/ksi*p->D[i]*((c_n-c) + p->z[i] * c_f * (phi_n_cp-phi_cp));
        } 
    }

        
    PetscScalar source = 0;

    if(v->bvreaction != NULL){
        BVReaction* er = v->bvreaction;
        double phi = in[vi+nos-1];
        double phi_cp = in[vi+nos-2];
        double eta = -1 * ((phi_cp - phi  + (p->B+v->Bmod)) - er->E0);
        double Rcoeff = -1 * er->n * er->k_0 * v->volume;

        source = Rcoeff * (in[vi + er->ox]/er->c_oe * exp(er->alpha * er->n*eta)
            - in[vi + er->red]/er->c_re*exp(-1*(1-er->alpha)*er->n*eta));
    }

    *dst = 0 - flux + source;
}

void TybrandtShuttleNPP::jac_cp_spec_local(Volume *v, PetscScalar *in,
        PetscScalar *dst, unsigned int i, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    ParameterStore *p = ctx->p;

    if(!v->stateIsActive[i]) return;

    PetscScalar c = in[vi+i];
    PetscScalar phi_cp = in[vi+nos-2];

    PetscScalar dc = 0;
    PetscScalar dphi_cp = 0, dphi = 0;
    unsigned int f;
    for(f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Volume* nv = v->nvol[f];
        Face* face = v->face[f];
        unsigned int ni = nv->solution_index;

        PetscScalar c_n, phi_n_cp, c_f;

        PetscScalar factor = A*p->D[i]/ksi;

        if( (face->bcond_type[i] == -1 || face->bcond_type[i] == 0) && nv->stateIsActive[i]){
            phi_n_cp = in[ni+nos-2];
            c_n = in[ni+i];
            c_f = 0.5 * (c + c_n);
            dc += factor * (1 - p->z[i] * 0.5 * (phi_n_cp-phi_cp));
            dphi_cp += factor * p->z[i] * c_f;

        } else if(face->bcond_type[i] == 3){
            phi_n_cp = nv->state[nos-2] - (p->B + v->Bmod);
            PetscScalar phi = in[vi+nos-1];
            c_n = (phi_n_cp - phi) * p->C_v;
            PetscScalar c_f = 0.5 * (c + c_n);

            dc += factor * (1 - p->z[i] * 0.5 * (phi_n_cp-phi_cp));
            dphi_cp += factor * (p->z[i] * c_f);
            dphi += factor * (p->z[i] * 0.5 * p->C_v * (phi_n_cp-phi_cp));
        }
    }
    dst[i] = dc;
    dst[nos-2] = dphi_cp;
    dst[nos-1] = dphi;

    if(v->bvreaction != NULL){
        BVReaction* er = v->bvreaction;
        double phi = in[vi+nos-1];
        double phi_cp = in[vi+nos-2];
        double eta = -1 * ( (phi_cp - phi + (p->B+v->Bmod)) - er->E0 );

        double Rcoeff = -1 * er->n * er->k_0 * v->volume;

        dst[er->ox] += Rcoeff * 1/er->c_oe * exp(er->alpha*er->n*eta);
        dst[er->red] += Rcoeff * -1/er->c_re * exp(-1*(1-er->alpha)*er->n*eta);

        double dRdphi = Rcoeff * er->n* (er->alpha * in[vi + er->ox]/er->c_oe * exp(er->alpha * er->n*eta)
            + (1-er->alpha) * in[vi + er->red]/er->c_re * exp(-1*(1-er->alpha)*er->n*eta));

        dst[nos-1] += dRdphi;
        dst[nos-2] += -1*dRdphi;
    }
}

void TybrandtShuttleNPP::jac_cp_spec_neighbour(Volume *v, PetscScalar *in,
        PetscScalar *dst, unsigned int i, unsigned int f, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    ParameterStore *p = ctx->p;
    Volume *nv = v->nvol[f];

    if(!v->stateIsActive[i]) return;
    if(!nv->stateIsActive[i]) return;

    PetscScalar dphi_cp = 0,dc = 0;
    unsigned int ni = nv->solution_index;

    PetscScalar c, phi_cp, c_n, phi_n_cp, c_f;
    phi_cp = in[vi+nos-2];
    phi_n_cp = in[ni+nos-2];

    c = in[vi+i];
    c_n = in[ni+i];
    c_f  = 0.5 * (c + c_n);
    
    PetscScalar A = v->face[f]->area;
    PetscScalar ksi = v->distance[f];

    PetscScalar factor = A*p->D[i]/ksi;

    dc = -factor * (1 + p->z[i] * 0.5 * (phi_n_cp-phi_cp));
    dst[i] = dc;

    dphi_cp = -factor * p->z[i] * c_f;

    dst[nos-2] = dphi_cp;
}


// Poisson CP

void TybrandtShuttleNPP::fnc_cp_pois(Volume *v, PetscScalar *in, PetscScalar *dst,
        Application *ctx)
{
    unsigned int vi = v->solution_index;
    unsigned int nos = v->num_states;
    ParameterStore *p = ctx->p;

    PetscScalar phi_cp = in[vi+nos-2];

    /* Source term */
    PetscScalar rho = 0, source;
    PetscScalar phi = in[vi+nos-1];
    rho = in[vi+0] * p->z[0];
    rho -= (phi_cp - phi) * p->C_v;

    source = v->volume * rho * p->debye;
    if(!v->stateIsActive[nos-2]) source = 0;
    if(!v->stateIsActive[nos-2]) source = v->volume * in[vi+0] * p->z[0] * p->debye;

    /* Flux term */
    PetscScalar flux = 0;
    for(unsigned int f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Volume* nv = v->nvol[f];
        unsigned int ni = nv->solution_index;
        Face* face = v->face[f];
        PetscScalar phi_n_cp;
        if(face->bcond_type[nos-2] == 0) phi_n_cp = nv->state[nos-2] - (p->B + v->Bmod);
        else phi_n_cp = in[ni+nos-2];

        if(face->bcond_type[nos-2] == 2)
            flux += 0;
        else 
            flux += A / ksi * (phi_n_cp - phi_cp);
    }
    *dst = flux + source;
}

void TybrandtShuttleNPP::jac_cp_pois_neighbour(Volume *v, PetscScalar *in,
        PetscScalar *dst, unsigned int f, Application *ctx)
{
    Face* face = v->face[f];
    Volume* nv = v->nvol[f];
    unsigned int nos = nv->num_states;

    if(face->bcond_type[nos-2] == 2)
        dst[nos-2] += 0;
    else
        dst[nos-2] = v->face[f]->area/v->distance[f];
}

void TybrandtShuttleNPP::jac_cp_pois_local(Volume *v, PetscScalar *in,
        PetscScalar *dst, Application *ctx)
{
    ParameterStore *p = ctx->p;
    unsigned int nos = v->num_states;

    PetscScalar dphi_cp = 0;

    unsigned int f;
    for(f = 0; f < v->num_faces; f++) {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Face* face = v->face[f];
        if(face->bcond_type[nos-2] == 2)
            dphi_cp += 0;
        else
            dphi_cp += -A/ksi;
    }
    dst[nos-2] = dphi_cp;

    if(v->stateIsActive[nos-2]) {
        dst[nos-1] = p->C_v * v->volume * p->debye;
        dst[nos-2] += -1*p->C_v * v->volume * p->debye;
        dst[0] = v->volume * p->debye * p->z[0];
    } else
        dst[0] = 0;
        dst[0] = v->volume * p->debye * p->z[0];
}