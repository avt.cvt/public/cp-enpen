#ifndef TYBRANDTMODELL_H
#define TYBRANDTMODELL_H

#include "Model.h"
#include <exception>

class TybrandtNPP : public Model{
public:
	void fnc_spec(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *);
	void fnc_pois(Volume *, PetscScalar *, PetscScalar *, Application *);
	void fnc_time(Volume *, PetscScalar *, PetscScalar *, PetscScalar *, unsigned int, Application *);
	void jac_spec_local(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *);
	void jac_pois_local(Volume *, PetscScalar *, PetscScalar *, Application *);
	void jac_time(Volume *, PetscScalar *, PetscScalar *, PetscScalar *, unsigned int i, Application *);
	void jac_spec_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, unsigned int, Application *);

	void jac_pois_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *);

	void fnc_cp_spec(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *);
	void jac_cp_spec_local(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *);
	void jac_cp_spec_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, unsigned int, Application *);

	void fnc_flux(Face *, Application *);

	void fnc_cp_pois(Volume *, PetscScalar *, PetscScalar *, Application *);
	void jac_cp_pois_local(Volume *, PetscScalar *, PetscScalar *, Application *);
	void jac_cp_pois_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *);
};

#endif

