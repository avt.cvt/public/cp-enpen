#include "BVReaction.h"

BVReaction::BVReaction(int num_species, int ox, int red, double c_oe, double c_re){
    this->mu.resize(num_species,0);
    this->nu.resize(num_species,0);
    this->ox = ox;
    this->mu[red] = -1;
    this->red = red;
    this->nu[ox] = 1;
    this->alpha = 0.5;

    this->k_0 = 1;
    this->E0 = 0;
    this->n = 1;

    this->c_oe = c_oe;
    this->c_re = c_re;
}