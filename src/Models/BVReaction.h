#ifndef BVREACTION_H
#define BVREACTION_H

#include <vector>

class BVReaction {
public:
    std::vector<int> mu;
    std::vector<int> nu;
    double k_0;
    double alpha;
    double c_oe;
    double c_re;
    double n;
    double E0;
    unsigned int ox,red;
    BVReaction(int num_species, int ox, int red, double c_oe, double r_oe);
};

#endif
