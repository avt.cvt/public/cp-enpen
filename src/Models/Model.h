#ifndef MODELL_H
#define MODELL_H

#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif
#include <petscsys.h>
#include <petscts.h>
#include <cmath>

class Model;
class Application;
#include "../Application.h"


class Model {

public:
	unsigned int num_add_vars = 2;
	unsigned int bs;

	virtual void fnc_spec(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *) = 0;
	virtual void fnc_pois(Volume *, PetscScalar *, PetscScalar *, Application *) = 0;
	virtual void fnc_time(Volume *, PetscScalar *, PetscScalar *, PetscScalar *, unsigned int, Application *) = 0;
	virtual void jac_spec_local(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *) = 0;
	virtual void jac_pois_local(Volume *, PetscScalar *, PetscScalar *, Application *) = 0;
	virtual void jac_time(Volume *, PetscScalar *, PetscScalar *, PetscScalar *, unsigned int i, Application *) = 0;
	virtual void jac_spec_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, unsigned int, Application *) = 0;
	virtual void jac_pois_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *) = 0;

	virtual void fnc_cp_spec(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *) = 0;
	virtual void jac_cp_spec_local(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *) = 0;
	virtual void jac_cp_spec_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, unsigned int, Application *) = 0;

	virtual void fnc_cp_pois(Volume *, PetscScalar *, PetscScalar *, Application *) = 0;
	virtual void jac_cp_pois_local(Volume *, PetscScalar *, PetscScalar *, Application *) = 0;
	virtual void jac_cp_pois_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *) = 0;

	virtual void fnc_flux(Face *, Application *) = 0;
};

#endif