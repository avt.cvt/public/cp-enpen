#include "Solver.h"

/* TS Interface */
PetscErrorCode cb_func_ts(TS ts, PetscReal t, Vec x, Vec xdot, Vec f,
                          void *context);
PetscErrorCode cb_jac_ts(TS ts, PetscReal t, Vec x, Vec xdot, PetscReal a,
                         Mat jac, Mat B, void *context);

PetscErrorCode vol_jac_ts(Volume *v, PetscScalar *in, PetscScalar *din,
                          Mat jac, Application *ctx, PetscReal a);

PetscErrorCode SolverData::init(unsigned int nv, unsigned int num_variables, Application *ctx)
{
    PetscErrorCode ierr;
    this->bs = nv; // can't change yet, some functions depend on it
    this->nv = nv;
    this->N = num_variables;

    ierr = VecCreate(PETSC_COMM_WORLD, &this->x);
    CHKERRXX(ierr);
    // ierr = VecSetBlockSize(this->x, this->bs); CHKERRXX(ierr);
    ierr = VecSetSizes(this->x, PETSC_DECIDE, this->N);
    CHKERRXX(ierr);
    ierr = VecSetType(this->x, VECMPI);
    CHKERRXX(ierr);

    ierr = VecDuplicate(this->x, &this->x1);
    CHKERRXX(ierr);
    ierr = VecDuplicate(this->x, &this->x2);
    CHKERRXX(ierr);
    ierr = VecDuplicate(this->x, &this->r);
    CHKERRXX(ierr);

    /* Create Jacobian */
    this->create_matrix();

    /* Create TS */
    ierr = TSCreate(PETSC_COMM_WORLD, &this->ts);
    CHKERRXX(ierr);
    ierr = TSSetProblemType(this->ts, TS_NONLINEAR);
    CHKERRXX(ierr);
    ierr = TSSetType(this->ts, TSBEULER);
    CHKERRXX(ierr);

    ierr = TSSetIFunction(this->ts, this->r, cb_func_ts, ctx);
    CHKERRXX(ierr);
    ierr = TSSetIJacobian(this->ts, this->J, this->J, cb_jac_ts, ctx);
    CHKERRXX(ierr);
    return 0;
}

void SolverData::copyValuesFromMesh(Mesh *mesh)
{
    PetscErrorCode ierr;

    PetscInt rstart, rend;
    ierr = VecGetOwnershipRange(this->x, &rstart, &rend);
    CHKERRXX(ierr);

    for (unsigned int vi = 0; vi < mesh->num_volumes; vi++)
    {
        Volume *v = mesh->volumes[vi];
        unsigned int nos = v->num_states;
        unsigned int si = v->solution_index;
        for (unsigned int i = 0; i < nos; i++)
        {
            if ((int)(si + i) < rend && (int)(si + i) >= rstart)
            {
                ierr = VecSetValue(this->x, si + i, v->state[i], INSERT_VALUES);
                CHKERRXX(ierr);
            }
        }
    }

    ierr = VecAssemblyBegin(this->x);
    CHKERRXX(ierr);
    ierr = VecAssemblyEnd(this->x);
    CHKERRXX(ierr);
}

void SolverData::copySolutionToMesh(Mesh *mesh)
{

    PetscErrorCode ierr;

    VecScatter sctr;
    Vec xglobal;
    ierr = VecScatterCreateToAll(this->x, &sctr, &xglobal);
    CHKERRXX(ierr);
    ierr = VecScatterBegin(sctr, this->x, xglobal, INSERT_VALUES,
                           SCATTER_FORWARD);
    CHKERRXX(ierr);
    ierr = VecScatterEnd(sctr, this->x, xglobal, INSERT_VALUES,
                         SCATTER_FORWARD);
    CHKERRXX(ierr);

    PetscScalar *x;
    ierr = VecGetArray(xglobal, &x);
    CHKERRXX(ierr);

    for (unsigned int vi = 0; vi < mesh->num_volumes; vi++)
    {
        Volume *v = mesh->volumes[vi];
        unsigned int si = v->solution_index;
        for (unsigned int i = 0; i < v->num_states; i++)
        {
            v->state[i] = x[si + i];
        }
    }

    ierr = VecRestoreArray(xglobal, &x);
    CHKERRXX(ierr);
    ierr = VecScatterDestroy(&sctr);
    CHKERRXX(ierr);
    ierr = VecDestroy(&xglobal);
    CHKERRXX(ierr);
}

PetscErrorCode SolverData::destroy()
{
    PetscErrorCode ierr;
    ierr = TSDestroy(&this->ts);
    CHKERRXX(ierr);
    ierr = VecDestroy(&this->x);
    CHKERRXX(ierr);
    ierr = VecDestroy(&this->x1);
    CHKERRXX(ierr);
    ierr = VecDestroy(&this->x2);
    CHKERRXX(ierr);
    ierr = VecDestroy(&this->r);
    CHKERRXX(ierr);
    ierr = MatDestroy(&this->J);
    CHKERRXX(ierr);
    return 0;
}

void SolverData::create_matrix()
{
    PetscErrorCode ierr;

    PetscInt rstart, rend;
    ierr = VecGetOwnershipRange(this->x, &rstart, &rend);
    CHKERRXX(ierr);
    PetscInt nr = rend - rstart;

    PetscInt *d_nnz, *o_nnz, *nnz;
    ierr = PetscMalloc(nr * sizeof(PetscInt), &d_nnz);
    CHKERRXX(ierr);
    ierr = PetscMalloc(nr * sizeof(PetscInt), &o_nnz);
    CHKERRXX(ierr);
    ierr = PetscMalloc(nr * sizeof(PetscInt), &nnz);
    CHKERRXX(ierr);
    memset(d_nnz, 0, nr * sizeof(PetscInt));
    memset(o_nnz, 0, nr * sizeof(PetscInt)); // PetscCalloc1 does exist

    for (int vi = rstart; vi < rend; vi++)
    {
        int i = vi - rstart;
        d_nnz[i] += 4 * this->nv;
        o_nnz[i] += 1 * this->nv;
    }
    MatCreate(PETSC_COMM_WORLD, &(this->J));
    MatSetSizes(this->J, PETSC_DECIDE, PETSC_DECIDE, this->N, this->N);
    MatSetType(this->J, MATMPIAIJ);
    MatMPIAIJSetPreallocation(this->J, 0, d_nnz, 0, o_nnz);

    MatSetOption(this->J, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);

    PetscInt m, n;
    MatGetSize(this->J, &m, &n);

    ierr = PetscFree(d_nnz);
    CHKERRXX(ierr);
    ierr = PetscFree(o_nnz);
    CHKERRXX(ierr);
}

PetscErrorCode cb_func_ts(TS ts, PetscReal t, Vec x, Vec xdot, Vec f,
                          void *context)
{
    PetscErrorCode ierr;
    Application *ctx = (Application *)context;
    Model *model = (Model *)ctx->model;

    VecScatter sctr;
    Vec xglobal, xdotglobal;
    ierr = VecScatterCreateToAll(x, &sctr, &xglobal);
    CHKERRXX(ierr);
    ierr = VecScatterBegin(sctr, x, xglobal, INSERT_VALUES, SCATTER_FORWARD);
    CHKERRXX(ierr);
    ierr = VecScatterEnd(sctr, x, xglobal, INSERT_VALUES, SCATTER_FORWARD);
    CHKERRXX(ierr);
    ierr = VecScatterDestroy(&sctr);
    CHKERRXX(ierr);

    ierr = VecScatterCreateToAll(xdot, &sctr, &xdotglobal);
    CHKERRXX(ierr);
    ierr = VecScatterBegin(sctr, xdot, xdotglobal, INSERT_VALUES, SCATTER_FORWARD);
    CHKERRXX(ierr);
    ierr = VecScatterEnd(sctr, xdot, xdotglobal, INSERT_VALUES, SCATTER_FORWARD);
    CHKERRXX(ierr);
    ierr = VecScatterDestroy(&sctr);
    CHKERRXX(ierr);

    PetscInt rstart, rend;
    ierr = VecGetOwnershipRange(x, &rstart, &rend);
    CHKERRXX(ierr);

    PetscScalar *in, *din, *out, *dst;
    ierr = VecGetArray(xglobal, &in);
    CHKERRXX(ierr);
    ierr = VecGetArray(xdotglobal, &din);
    CHKERRXX(ierr);
    ierr = VecGetArray(f, &out);
    CHKERRXX(ierr);

    Mesh *mesh = ctx->mesh;
    for (unsigned int vi = 0; vi < mesh->num_volumes; vi++)
    {
        Volume *v = mesh->volumes[vi];
        unsigned int si = v->solution_index;
        for (unsigned int i = 0; i < v->num_states; i++)
        {
            if ((int)(si + i) < rend && (int)(si + i) >= rstart)
            {
                dst = &out[si + i - rstart];
                *dst = 0;
                if (i == v->num_states - 1)
                {
                    model->fnc_pois(v, in, &dst[0], ctx);
                }
                else if (i == v->num_states - 2)
                {
                    model->fnc_cp_pois(v, in, &dst[0], ctx);
                }
                else if (i == 0)
                {
                    model->fnc_cp_spec(v, in, &dst[0], i, ctx);
                    PetscScalar t_term;
                    model->fnc_time(v, in, din, &t_term, i, ctx);
                    dst[0] += t_term;
                }
                else
                {
                    model->fnc_spec(v, in, &dst[0], i, ctx);
                    PetscScalar t_term;
                    model->fnc_time(v, in, din, &t_term, i, ctx);
                    dst[0] += t_term;
                }
            }
        }
    }

    ierr = VecRestoreArray(xglobal, &in);
    CHKERRXX(ierr);
    ierr = VecRestoreArray(xdotglobal, &din);
    CHKERRXX(ierr);
    ierr = VecRestoreArray(f, &out);
    CHKERRXX(ierr);

    ierr = VecDestroy(&xglobal);
    CHKERRXX(ierr);
    ierr = VecDestroy(&xdotglobal);
    CHKERRXX(ierr);
    return 0;
}

PetscErrorCode cb_jac_ts(TS ts, PetscReal t, Vec x, Vec xdot, PetscReal a,
                         Mat jac, Mat B, void *context)
{
    PetscErrorCode ierr;
    Application *ctx = (Application *)context;

    VecScatter sctr;
    Vec xglobal, xdotglobal;
    ierr = VecScatterCreateToAll(x, &sctr, &xglobal);
    CHKERRXX(ierr);
    ierr = VecScatterBegin(sctr, x, xglobal, INSERT_VALUES, SCATTER_FORWARD);
    CHKERRXX(ierr);
    ierr = VecScatterEnd(sctr, x, xglobal, INSERT_VALUES, SCATTER_FORWARD);
    CHKERRXX(ierr);
    ierr = VecScatterDestroy(&sctr);
    CHKERRXX(ierr);

    ierr = VecScatterCreateToAll(xdot, &sctr, &xdotglobal);
    CHKERRXX(ierr);
    ierr = VecScatterBegin(sctr, xdot, xdotglobal, INSERT_VALUES, SCATTER_FORWARD);
    CHKERRXX(ierr);
    ierr = VecScatterEnd(sctr, xdot, xdotglobal, INSERT_VALUES, SCATTER_FORWARD);
    CHKERRXX(ierr);
    ierr = VecScatterDestroy(&sctr);
    CHKERRXX(ierr);

    PetscInt rstart, rend;
    ierr = MatGetOwnershipRange(jac, &rstart, &rend);
    CHKERRXX(ierr);

    PetscScalar *in, *din;
    ierr = VecGetArray(xglobal, &in);
    CHKERRXX(ierr);
    ierr = VecGetArray(xdotglobal, &din);
    CHKERRXX(ierr);

    ierr = MatZeroEntries(jac);
    CHKERRXX(ierr);
    Mesh *mesh = ctx->mesh;
    for (unsigned int vi = 0; vi < mesh->num_volumes; vi++)
    {
        Volume *v = mesh->volumes[vi];
        if ((int)v->solution_index < rend && (int)(v->solution_index + v->num_states) >= rstart)
        {
            vol_jac_ts(ctx->mesh->volumes[vi], in, din, jac, ctx, a);
        }
    }

    ierr = MatAssemblyBegin(jac, MAT_FINAL_ASSEMBLY);
    CHKERRXX(ierr);
    ierr = MatAssemblyEnd(jac, MAT_FINAL_ASSEMBLY);
    CHKERRXX(ierr);

    ierr = VecRestoreArray(xglobal, &in);
    CHKERRXX(ierr);
    ierr = VecRestoreArray(xdotglobal, &din);
    CHKERRXX(ierr);

    ierr = VecScatterDestroy(&sctr);
    CHKERRXX(ierr);
    ierr = VecDestroy(&xglobal);
    CHKERRXX(ierr);
    ierr = VecDestroy(&xdotglobal);
    CHKERRXX(ierr);
    return 0;
}

PetscErrorCode vol_jac_ts(Volume *v, PetscScalar *in, PetscScalar *din,
                          Mat jac, Application *ctx, PetscReal a)
{
    PetscErrorCode ierr;
    Model *model = (Model *)ctx->model;

    PetscInt rstart, rend;
    ierr = MatGetOwnershipRange(jac, &rstart, &rend);
    CHKERRXX(ierr);

    PetscInt nas = v->num_states;

    PetscScalar *vec;
    ierr = PetscMalloc(ctx->nv * sizeof(PetscScalar), &vec);
    CHKERRXX(ierr);
    memset(vec, 0, ctx->nv * sizeof(PetscScalar));

    /* These will hold the indices to set */
    PetscInt *idx, *cidx;
    ierr = PetscMalloc(nas * sizeof(PetscInt), &idx);
    CHKERRXX(ierr);
    ierr = PetscMalloc(ctx->nv * sizeof(PetscInt), &cidx);
    CHKERRXX(ierr);
    memset(idx, 0, nas * sizeof(PetscInt));
    memset(cidx, 0, ctx->nv * sizeof(PetscInt));

    PetscInt vi = (PetscInt)v->solution_index;
    PetscInt row = 0;

    PetscMPIInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
    if ((rend - vi) < nas)
    {
        PetscPrintf(PETSC_COMM_WORLD, "[%d] Stitch at vi %d required\n", rank, vi);
    }
    else if ((vi + nas - rstart) < nas)
    {
        PetscPrintf(PETSC_COMM_WORLD, "[%d] Stitch at vi %d required\n", rank, vi);
    }

    /* Local volume */
    for (int i = 0; i < nas; i++)
        idx[i] = vi + i;
    for (unsigned int i = 0; i < (unsigned int)nas - 2; i++)
    {

        if (i == 0)
            model->jac_cp_spec_local(v, in, vec, i, ctx);
        else
            model->jac_spec_local(v, in, vec, i, ctx);

        PetscScalar t_term;
        model->jac_time(v, in, din, &t_term, i, ctx);
        vec[i] += a * t_term;

        row = vi + i;
        if (row >= rstart && row < rend)
            ierr = MatSetValues(jac, 1, &row, nas, idx, vec, INSERT_VALUES);
        CHKERRXX(ierr);
        memset(vec, 0, ctx->nv * sizeof(PetscScalar));
    }

    model->jac_cp_pois_local(v, in, vec, ctx);
    row = vi + nas - 2;
    if (row >= rstart && row < rend)
        ierr = MatSetValues(jac, 1, &row, nas, idx, vec, INSERT_VALUES);
    CHKERRXX(ierr);
    memset(vec, 0, ctx->nv * sizeof(PetscScalar));

    model->jac_pois_local(v, in, vec, ctx);
    row = vi + nas - 1;
    if (row >= rstart && row < rend)
        ierr = MatSetValues(jac, 1, &row, nas, idx, vec, INSERT_VALUES);
    CHKERRXX(ierr);

    /* All neighbouring volumes */
    for (unsigned int f = 0; f < v->num_faces; f++)
    {
        Volume *nv = v->nvol[f];
        PetscInt cnas = nv->num_states;
        unsigned int ni = nv->solution_index;

        /* Ignore boundary conditions */
        if (!nv->isGhostNode)
        {
            memset(vec, 0, ctx->nv * sizeof(PetscScalar));
            memset(cidx, 0, ctx->nv * sizeof(PetscInt));

            for (int i = 0; i < cnas; i++)
                cidx[i] = ni + i;
            for (unsigned int i = 0; i < (unsigned int)nas - 2; i++)
            {
                if (i == 0)
                    model->jac_cp_spec_neighbour(v, in, vec, i, f, ctx);
                else
                    model->jac_spec_neighbour(v, in, vec, i, f, ctx);

                row = (vi + i);
                if (row >= rstart && row < rend)
                    ierr = MatSetValues(jac, 1, &row, cnas, cidx, vec, INSERT_VALUES);
                CHKERRXX(ierr);
                memset(vec, 0, ctx->nv * sizeof(PetscScalar));
            }

            model->jac_pois_neighbour(v, in, vec, f, ctx);
            row = vi + nas - 1;
            if (row >= rstart && row < rend)
                ierr = MatSetValues(jac, 1, &row, cnas, cidx, vec, INSERT_VALUES);
            CHKERRXX(ierr);
            memset(vec, 0, ctx->nv * sizeof(PetscScalar));
            model->jac_cp_pois_neighbour(v, in, vec, f, ctx);
            row = vi + nas - 2;
            if (row >= rstart && row < rend)
                ierr = MatSetValues(jac, 1, &row, cnas, cidx, vec, INSERT_VALUES);
            CHKERRXX(ierr);
        }
    }

    ierr = PetscFree(vec);
    CHKERRXX(ierr);
    ierr = PetscFree(idx);
    CHKERRXX(ierr);
    ierr = PetscFree(cidx);
    CHKERRXX(ierr);
    return 0;
}