#include "Mesh1D.h"

Mesh1D::Mesh1D(std::string filename,unsigned int nv,double mesh_unit):Mesh(filename,nv,mesh_unit){

    std::ifstream mf(filename);

    this->readZones(mf);

    /* Read nodes */
    Node *nodes;
    unsigned int num_nodes;
    this->readNodes(mf, &nodes, num_nodes);
    for(unsigned int i = 0; i < num_nodes; i++) this->size.expand(nodes[i]);
    this->readUntil(mf,"$Elements");

    /* Allocate Memory to collect faces */
    Face** facelist = new Face*[num_nodes+1]();

    /* Read elements */
    unsigned int num_elems, i;
    mf >> num_elems;
    /* Allocate memory for elements */
    this->volumes = (Volume **)malloc(num_elems * sizeof(Volume *));
    if(!this->volumes) throw std::runtime_error("mesh_parse_1D malloc");

    // Parse Elements
    std::string line;
    for(i = 0; i < num_elems; i++){
        /* Parse element in line */
        unsigned int index, type, num_tags;
        /* Node indices */
        unsigned int a, b, tmp;

        mf >> index >> type >> num_tags;
        unsigned int *tags = new unsigned int[num_tags]();
        for(unsigned int i = 0; i < num_tags; i++) mf >> tags[i];
        if(tags[0] > this->zones.size()) throw std::runtime_error("invalid zone index");

        Volume* vol;
        switch(type) {
            case GMSH_LINE:
                /* Read two nodes making a line */
                mf >> a >> b;
                /* Craft volume from line */
                if(a>b) {
                    tmp = b; b = a; a = tmp;
                }
                // Volume* vol = 
                vol = this->addVolume(nodes, facelist, a-1, b-1);
                this->zones[tags[0]-1]->add_volume(vol);
                break;
            case GMSH_NODE:
                break;
            default:
                fprintf(stderr, "Element type not supported for 1D mesh: %i\n",type);
        }
        delete[] (tags);
    }

    delete[] (nodes);
    
    /* Post processing */
    // Step 0: Realloc to the actual num_volumes
    this->volumes = (Volume**)realloc(this->volumes,this->num_volumes*sizeof(Volume *));
    // Step 1: Collect all faces in facelist
    unsigned int num_faces = 0;
    for(i = 0; i < num_nodes+1; i++) {
        if(!facelist[i]) continue;
        num_faces++;
    }
    this->faces = new Face*[num_faces]();

    for(i = 0; i < num_nodes+1; i++) {
        if(!facelist[i]) continue;
        this->faces[this->num_faces++] = facelist[i];
    }
    delete[] (facelist);
    // Step 2: Create the boundaries
    this->setup_boundaries();
    // Step 3: Final meshing and calculated distances

    this->connect_volumes();

    for(Zone* z : this->zones)
        z->identify_boundaries();

    mf.close();
}

Volume* Mesh1D::addVolume(Node *nodes, Face **facelist,
        const unsigned int a, const unsigned int b) {

    double A = MESH_EXTENSION_LENGTH * MESH_EXTENSION_LENGTH;
    double V = A * nodes[a].distanceTo(nodes[b]);

    /* Create the volume in memory */
    Volume *vol = new Volume(2, V, this->num_volumes, this->num_states);

    this->volumes[this->num_volumes++] = vol;

    /* Calculate center of the volume */
    vol->center.x = (nodes[a].x+nodes[b].x)/2;
    vol->center.y = (nodes[a].y+nodes[b].y)/2;
    vol->center.z = (nodes[a].z+nodes[b].z)/2;

    /* Identify or create faces for this volume */
    if(!facelist[a]) {
        facelist[a] = new Face(A, this->num_states);
        facelist[a]->nvol[0] = vol;
        facelist[a]->center = nodes[a];
        facelist[a]->setType((face_type_t) this->size.nodeBoundary(&nodes[a]));
    } else facelist[a]->nvol[1] = vol;
    if(!facelist[b]) {
        facelist[b] = new Face(A, this->num_states);
        facelist[b]->nvol[0] = vol;
        facelist[b]->center = nodes[b];
        facelist[b]->setType((face_type_t) this->size.nodeBoundary(&nodes[b]));
    } else facelist[b]->nvol[1] = vol;

    /* Connect the volume and face */
    vol->face[0] = facelist[a];
    vol->face[1] = facelist[b];
    return vol;
}

void Mesh1D::setup_boundaries(){

    // Identify them

    for(unsigned int i = 0; i < this->num_faces; i++) {
        Face *f = this->faces[i];
        if(f->type != FACETYPE_INTERNAL) this->boundary_faces[f->type].push_back(f);
    }

    // Create ghost nodes

    for(auto& b: this->boundary_faces){
        for(auto& f: b){
            Volume *v, *nv;
            nv = f->nvol[0];
            v = new Volume(1, nv->volume, 0, this->num_states, true);
            f->nvol[1] = v;
            v->nvol[0] = nv;

            // TODO: switch on type

            if(nv->index == 0) { 
                v->center.x = nv->center.x - 0.5e-6;
            } else {
                v->center.x = nv->center.x + 0.5e-6;
            }
            this->ghost_nodes.push_back(v);
            this->boundaries[f->type].push_back(v);
        }
    }
}