#ifndef VOLUME_H
#define VOLUME_H
#include <cmath>
#include <list>
#include <vector>
#include <stdexcept>
#include <climits>

#include "Node.h"

typedef enum vol_type {VOLTYPE_INTERNAL,
                       VOLTYPE_MEMBRANE,
                       VOLTYPE_BULK,
                       VOLTYPE_DOWNSTREAM,
                       VOLTYPE_ELECTRODE} vol_type_t;

class Face;
class BVReaction;

class Volume{
public:
    unsigned int num_faces;
    unsigned int index;
    vol_type_t type;
    double *state;          /* State of this volume element */
    Node center;   /* Geometrical center of volume element */
    double volume;          /* The dimensionless volume of this element */
    double charge;          /* The dimensionless background charge */
    double alpha;           /* The membrane diffusion factor */
    Face **face;   /* Associated faces */
    Volume **nvol; /* Neighbouring volumes */
    double *distance;       /* Neighbour-associated distance */
    unsigned int zone_id;
    std::vector<bool> stateIsActive;
    unsigned int solution_index;
    bool isGhostNode = false;
    unsigned int num_states;
    double Bmod; // local modification of B. additive
    BVReaction* bvreaction;


    Volume(const unsigned int num_faces,
        const double volume,
        const unsigned int index,
        const unsigned int num_states,
        bool isBoundary = false);
    ~Volume();
};

#endif