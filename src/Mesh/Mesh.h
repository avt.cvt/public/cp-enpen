#ifndef MESH_H
#define MESH_H
#include <cmath>
#include <list>
#include <vector>
#include "Volume.h"
#include "Face.h"
#include "Node.h"
#include "Zone.h"
#include "Box.h"
#include <iostream>
#include <fstream>
#include <string>
#include <petscsys.h>
#include <map>
#include "Boundary.h"

#define GMSH_LINE 1
#define GMSH_TRIANGLE 2
#define GMSH_NODE 15

#define MESH_EXTENSION_LENGTH 1

typedef enum elem_type {ELEM_LINE, ELEM_TRIANGLE, ELEM_NODE} elem_type_t;

typedef enum bcond_type {BCOND_0, BCOND_2, BCOND_4} bcond_type_t;

class Mesh{
private: 
    bool initialized = false; // prevent editing of mesh structure after model has been initialized
protected:
    // Mesh Parsing
    void readUntil(std::ifstream& meshfile, std::string line);
    void readZones(std::ifstream& meshfile);
    void readNodes(std::ifstream& meshfile, Node **nodes,
        unsigned int& num_nodes);

    void connect_volumes();
public:
    char* file;
    struct meshdef_s *def;
    double mesh_unit;
    Box size;
    unsigned int num_volumes;
    Volume **volumes;
    unsigned int num_faces;
    Face **faces;
    std::vector<std::list<Volume*> > boundaries;
    std::vector<std::list<Face*> > boundary_faces;
    std::list<Volume*> ghost_nodes;
    std::vector<Zone*> zones;
    int num_states;

    Mesh(std::string filename,unsigned int nv, double mesh_unit = 1e-6);
    ~Mesh();

    void initialize();
    void calc_solution_indices();

    // Mesh User Interface
    void set_state(double *state, unsigned int len);
    void set_boundary_state(int boundary, double *state,unsigned int len);
    void modify_boundary_state(int boundary, double state, unsigned int index);

    void setBoundaryCondition(std::list<Face*>& faces, int *bcond_type);

    Zone* getZone(std::string name);

    unsigned int numVariables();
    bool isInitialized();
};

#endif
