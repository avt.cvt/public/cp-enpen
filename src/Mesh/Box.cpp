#include "Box.h"
#include <math.h>

Box::Box(){
    this->a = Node(NAN,NAN,NAN);
    this->b = Node(NAN,NAN,NAN);
}

bool Box::contains(Node n)
{
    if(n.x >= this->a.x && n.x <= this->b.x &&
       n.y >= this->a.y && n.y <= this->b.y &&
       n.z >= this->a.z && n.z <= this->b.z) return 1;
    return 0;
}

void Box::expand(Node n){
    if(isnan(this->b.x) || n.x > this->b.x) this->b.x = n.x;
    if(isnan(this->a.x) || n.x < this->a.x) this->a.x = n.x;

    if(isnan(this->b.y) || n.y > this->b.y) this->b.y = n.y;
    if(isnan(this->a.y) || n.y < this->a.y) this->a.y = n.y;

    if(isnan(this->b.z) || n.z > this->b.z) this->b.z = n.z;
    if(isnan(this->a.z) || n.z < this->a.z) this->a.z = n.z;
}

int Box::nodeBoundary(Node *a){
    if(a->x == this->a.x) return FACETYPE_BOUNDARY_LEFT;
    if(a->x == this->b.x) return FACETYPE_BOUNDARY_RIGHT;
    // disabled for 1D
    // if(a->y == s->a.y) return FACETYPE_BOUNDARY_TOP;
    // if(a->y == s->b.y) return FACETYPE_BOUNDARY_BOTTOM;
    return FACETYPE_INTERNAL;
}