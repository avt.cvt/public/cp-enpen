#include "Zone.h"
#include <iostream>

Zone::Zone(std::string name, unsigned int index, unsigned int num_states){
    this->name = name;
    this->index = index;
    this->boundaries.resize(4);
    this->alpha = 1;
    this->charge = 0;
    this->Bmod = 0;

    this->num_states = num_states;

    this->states = new double[num_states]();

    this->activeStates.reserve(num_states);
    for(unsigned int i=0;i<num_states;i++)
        this->activeStates.push_back(i);

    this->stateIsActive.resize(num_states,true);

    this->bvreaction = NULL;
}

void Zone::applyActiveStates(){
    for(auto& v: this->volumes){
        v->stateIsActive = this->stateIsActive;
        // std::fill(v->stateIsActive.begin(),v->stateIsActive.end(),false);
        // for(auto& s: this->activeStates){
        //     v->stateIsActive[s] = true;
        // }
    }
}

Zone::~Zone(){
    if(this->states) delete[] this->states;
}

void Zone::add_volume(Volume* v){
    this->volumes.push_back(v);
    v->zone_id = this->index;

    for(unsigned int i=0;i<v->num_faces;i++)
        this->bounds.expand(v->face[i]->center);
}

void Zone::applyCharge(double charge){
    this->charge = charge;
    for (auto & v : this->volumes){
        v->charge = charge;
    }
}

void Zone::updateVolumeProperties(){
    for (auto & v : this->volumes){
        v->charge = this->charge;
        v->alpha = this->alpha;
        v->Bmod = this->Bmod;
        v->bvreaction = this->bvreaction;
    }
}


void Zone::applyToVolumes(){
    for (auto & v : this->volumes){
        v->type = (vol_type_t) this->type;
        v->charge = this->charge;
        v->alpha = this->alpha;
        v->Bmod = this->Bmod;
        v->bvreaction = this->bvreaction;
        for(unsigned int i=0;i<this->num_states-1;i++) {
            if(this->states) v->state[i] = this->states[i];
        }
    }
    this->applyActiveStates();
}

void Zone::identify_boundaries(){
    for(auto& v : this->volumes){
        for(unsigned int i=0;i<v->num_faces;i++){
            face_type_t type = (face_type_t) this->bounds.nodeBoundary(&(v->face[i]->center));
            if(type == FACETYPE_INTERNAL) continue;
            this->boundaries[type].push_back(v->face[i]);
        }
    }
}

std::list<Face*>& Zone::getLeftBoundary(){
    return this->boundaries[FACETYPE_BOUNDARY_LEFT];
}

std::list<Face*>& Zone::getRightBoundary(){
    return this->boundaries[FACETYPE_BOUNDARY_RIGHT];
}