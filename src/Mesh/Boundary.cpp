#include "Boundary.h"

void Boundary::addFace(Face* face){
    if(face->type == FACETYPE_INTERNAL) return; // not affecting internal faces for now
    this->faces.push_back(face);
}

void Boundary::setBcondType(int* bcond_type){
    for (Face* f : this->faces)
        for(unsigned int i = 0; i < f->num_states; i++)
            f->bcond_type[i] = bcond_type[i];
}

void Boundary::setFullBcondState(double* state){
    for (Face* f : this->faces){
        Volume* gn = (f->nvol[0]->isGhostNode) ? f->nvol[0] : f->nvol[1];
        for(unsigned int i = 0; i < gn->num_states; i++ )
            gn->state[i] = state[i];
    }
}

void Boundary::setBcondState(unsigned int index, double state){
    for (Face* f : this->faces){
        Volume* gn = (f->nvol[0]->isGhostNode) ? f->nvol[0] : f->nvol[1];
        gn->state[index] = state;
    }
}