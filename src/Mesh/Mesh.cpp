#include "Mesh.h"

Mesh::Mesh(std::string filename,unsigned int nv, double mesh_unit){

    this->num_volumes = 0;
    this->num_faces = 0;
    this->volumes = NULL;
    this->faces = NULL;

    this->boundaries.resize(4);
    this->boundary_faces.resize(4);

    this->mesh_unit = mesh_unit;
    this->num_states = nv;
}

void Mesh::readZones(std::ifstream& meshfile){
    this->readUntil(meshfile, "$PhysicalNames");

    unsigned int num_zones;
    meshfile >> num_zones;

    this->zones.reserve(num_zones);

    for(unsigned int i=0;i<num_zones;i++){
        std::string name; int skip;
        meshfile >> skip >> skip >> name;
        name = name.substr(1, name.size() -2);
        PetscPrintf(PETSC_COMM_WORLD, "Found zone: %s\n", name.c_str());

        this->zones.push_back(new Zone(name, i, this->num_states));
    }
}

void Mesh::readUntil(std::ifstream& meshfile, std::string line){
    std::string l;
    while(std::getline(meshfile, l)) if(l.find(line) != std::string::npos) return;
    throw std::runtime_error("Error reading mesh\n");
}

Zone* Mesh::getZone(std::string name){
    for (Zone* z : this->zones) {
        if (z->name.compare(name) == 0) return z;
    }
    throw std::runtime_error("Could not find zone " + name);
}

void Mesh::connect_volumes(){

    for(unsigned int i = 0; i < this->num_volumes; i++) {
        Volume *v = this->volumes[i];
        for(unsigned int j = 0; j < v->num_faces; j++) {
            Face *f = v->face[j];
            if(!f->nvol[0]||!f->nvol[1]) throw std::runtime_error("[!!] Face not properly connected\n");

            v->nvol[j] = (f->nvol[0] == v) ? f->nvol[1] : f->nvol[0];

            /* Calculate distance between volumes */
            if(v->nvol[j]->isGhostNode) 
                v->distance[j] = v->center.distanceTo(f->center);
            else 
                v->distance[j] = v->center.distanceTo((v->nvol[j]->center));
        }
    }
}

void Mesh::initialize()
{
    if(this->initialized) throw std::runtime_error("Meshes cannot be reused at this point.");

    this->calc_solution_indices();

    this->initialized = true;
}

bool Mesh::isInitialized(){
    return this->initialized;
}

void Mesh::calc_solution_indices(){ 
    unsigned int index = 0;
    for(unsigned int vi = 0; vi<this->num_volumes;vi++){
        this->volumes[vi]->solution_index = index; //wieso kann hier nicht der index verwendet werden sondern solution index?
        index += this->volumes[vi]->num_states;
    }
}

void Mesh::set_state(double *state, unsigned int len)
{
    for(unsigned int vi = 0; vi < this->num_volumes; vi++) {
        memcpy(this->volumes[vi]->state, state, len * sizeof(double)); //copys bytes from from volume.state to state (size of copy =size of double)
    }
    // also set zones
    for(Zone* z: this->zones)
        memcpy(z->states, state, len * sizeof(double)); // todo: check if this is safe
}

void Mesh::set_boundary_state(int boundary, double *state,
        unsigned int len)
{
    for (auto const& v : this->boundaries[boundary]) {
        memcpy(v->state, state, len * sizeof(double));
    }
}

void Mesh::setBoundaryCondition(std::list<Face*>& faces, int *bcond_type){
    for (auto& f : faces) {
        for (int i = 0; i < this->num_states; i++) {
            f->bcond_type[i] = bcond_type[i];
        }
    }
}


void Mesh::modify_boundary_state(int boundary, double state, unsigned int index) //Is there a difference between boundry state and bounrdary condition?
{
    for (auto const& v : this->boundaries[boundary]) {
        v->state[index] = state; 
    }
}


Mesh::~Mesh()
{
    unsigned int i;
    for(i = 0; i < this->num_volumes; i++) {
        delete (this->volumes[i]);
    }
    for(i = 0; i < this->num_faces; i++) {
        delete (this->faces[i]);
    }
    for(Volume* gn: this->ghost_nodes)
        delete gn;
    free(this->volumes);
    delete[] (this->faces);
    for(Zone* z: this->zones)
        delete z;
}

/* IO */
void Mesh::readNodes(std::ifstream& meshfile, Node **nodes,
        unsigned int& num_nodes)
{
    this->readUntil(meshfile, "$Nodes");
    meshfile >> num_nodes;
    PetscPrintf(PETSC_COMM_WORLD, "Nodes: %d\n", num_nodes);

    *nodes = new Node[num_nodes]();

    int skip;
    for(unsigned int i = 0; i < num_nodes; i++){
        Node* node = &(*nodes)[i];
        meshfile >> skip >> node->x >> node->y >> node->z;
    }
}

unsigned int Mesh::numVariables(){
    Volume* lastVolume = this->volumes[this->num_volumes-1];
    return lastVolume->solution_index + lastVolume->num_states;
}