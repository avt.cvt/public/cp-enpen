### VARIABLES

PETSC_DIR=petsc-3.10.5/arch-linux2-c-debug

ifeq ($(wildcard petsc-3.10.5),) 
	ifdef R_SLURM_ROLES
		PETSC_DIR=/usr/local_rwth/sw/petsc/3.13.5/intel_19.1.2.254-intelmpi_2019.8.254
	endif
endif

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	PETSC_DIR=/usr/local/Cellar/petsc/3.16.5
endif

include $(PETSC_DIR)/lib/petsc/conf/petscvariables
CFLAGS=-Werror -ggdb -Wall -D_GNU_SOURCE -Wno-nullability-completeness
INCLUDES=$(PETSC_CC_INCLUDES)
#LD_LIBRARY_PATH:=$(LD_LIBRARY_PATH):$(PETSC_DIR)/lib
#LD_LIBRARY_PATH:=$(PETSC_DIR)/lib
LDFLAGS=-L$(LD_LIBRARY_PATH) -L$(PETSC_DIR)/lib -lm $(PETSC_LIB_BASIC)
ifeq ($(UNAME_S),Darwin)
	LDFLAGS=-L$(PETSC_DIR)/lib -lm $(PETSC_LIB_BASIC)
endif

GMSH = gmsh

export LD_LIBRARY_PATH := $(LD_LIBRARY_PATH):$(PETSC_DIR)/lib

### RUNTIME OPTIONS
MPIOPTS=-np 1 # currently no parallel support

SRC := $(wildcard src/*.cpp) $(wildcard src/**/*.cpp) 
#SRC := $(filter-out src/bar.cpp, $(SRC))
OBJ := $(SRC:%.cpp=obj/%.o)


# Meshes
GEO := $(wildcard meshdata/*-1D.geo)
MSH := $(GEO:%.geo=%.msh)


# Systems
SYS := $(wildcard sysdata/*.sys)
DEF := $(SYS:%.sys=%.def)

# PetSc Options
TOL=-snes_atol 1e-8 -snes_rtol 1e-30 -snes_stol 1e-16 -ts_pseudo_fatol 1e-8 -ts_pseudo_frtol 1e-30
MONITOR=-snes_converged_reason #-snes_monitor 
OPTS=$(MONITOR) $(TOL) -snes_max_it 10 # -info

.PHONY: clean all debug valgrind test sys
all: memristor sys

run: sys memristor
	@rm -rf outdata/memristor2
	@mv outdata/memristor outdata/memristor2 || true # cache last result
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./memristor $(OPTS) 

run-parallel: sys memristor
	$(LDPATH) $(MPIEXEC) -n 2 ./memristor $(OPTS) 

### PATTERNS
obj/%.o: %.cpp
	@echo "[CC] >>> "$<
	@mkdir -p $(dir $@)
	@$(CXX) $(CFLAGS) $(INCLUDES) -std=c++11 -c $< -o $@ 
%1D.msh: %1D.geo
	@echo "[GM] >>> "$<
	$(GMSH) -1 -format msh22 $< -o $@ 
	### Gmsh -1 -format msh22 input.geo -o output.msh

%.def: %.sys
	@echo "[DG] >>> "$<
	@sysdata/defgen3.py $< > $@

### MESHES
geo: $(MSH)
### SYSTEMS
sys: $(DEF)

define macos-firewall-register
  @APP=$(call abspath, $(1)); \
    FW=/usr/libexec/ApplicationFirewall/socketfilterfw; \
    sudo $$FW --block $$APP
endef

### APPLICATIONS

ts-api: $(OBJ) obj/ts-api.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/ts-api.o -o $@  $(LDFLAGS)
	$(call macos-firewall-fix,$@)

ts-api-run: sys ts-api
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./ts-api $(OPTS) 

ts-api-reset: ts-api
	rm -rf outdata/ts-api-default
	$(LDPATH) $(MPIEXEC) -n 1 ./ts-api

tybrandt: $(OBJ) obj/tybrandt.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/tybrandt.o -o $@  $(LDFLAGS)

tybrandt-run: sys tybrandt
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./tybrandt $(OPTS) 

tybrandt2T: $(OBJ) obj/tybrandt2T.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/tybrandt2T.o -o $@  $(LDFLAGS)

tybrandt2T-run: sys tybrandt2T
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./tybrandt2T $(OPTS) 

tybrandt-shuttle: $(OBJ) obj/tybrandt-shuttle.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/tybrandt-shuttle.o -o $@  $(LDFLAGS)

tybrandt-shuttle-run: sys tybrandt-shuttle
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./tybrandt-shuttle $(OPTS) 

tybrandt-shuttle2: $(OBJ) obj/tybrandt-shuttle2.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/tybrandt-shuttle2.o -o $@  $(LDFLAGS)

tybrandt-shuttle2-run: sys tybrandt-shuttle2
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./tybrandt-shuttle2 $(OPTS) 

tybrandt-shuttle-solid: $(OBJ) obj/tybrandt-shuttle-solid.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/tybrandt-shuttle-solid.o -o $@  $(LDFLAGS)

tybrandt-shuttle-solid-run: sys tybrandt-shuttle-solid
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./tybrandt-shuttle-solid $(OPTS) 


dissociation: $(OBJ) obj/dissociation.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/dissociation.o -o $@  $(LDFLAGS)

dissociation-run: sys dissociation
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./dissociation $(OPTS) 

tsimpedance: $(OBJ) obj/tsimpedance.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/tsimpedance.o -o $@  $(LDFLAGS)

tsimpedance-run: sys tsimpedance
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./tsimpedance eis $(FREQUENCY) $(OPTS) 

tybrandtElSpeed: $(OBJ) obj/tybrandtElSpeed.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/tybrandtElSpeed.o -o $@  $(LDFLAGS)

tybrandtElSpeed-run: geo sys tybrandtElSpeed
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./tybrandtElSpeed $(CHANNEL) $(OPTS) 

tybrandtpulse: $(OBJ) obj/tybrandtpulse.o
	@echo "[LD] >>> "$@
	$(CXX) $(OBJ) obj/tybrandtpulse.o -o $@  $(LDFLAGS)

tybrandtpulse-run: sys tybrandtpulse
	@echo "[EX] >>> "$<
	$(LDPATH) $(MPIEXEC) -n 1 ./tybrandtpulse $(OPTS) 

### DEBUGGING
debug: sys memristor
	@echo "[DB] >>> "$<
	$(LDPATH) gdb ./memristor
	#lldb for mac

debug-parallel:
	$(LDPATH) $(MPIEXEC) -n 3 xterm -e gdb ./memristor

valgrind-parallel:
	$(LDPATH) $(MPIEXEC) -n 2 valgrind ./memristor -on_error_attach_debugger

valgrind: memristor
	@echo "[VG] >>> "$<
	@$(LDPATH) valgrind --leak-check=full ./memristor # --track-origins=yes

valgrind-nvrt: nvrt
	@echo "[VG] >>> "$<
	@$(LDPATH) valgrind --leak-check=full ./nvrt # --track-origins=yes

callgrind-nvrt: nvrt
	@echo "[VG] >>> "$<
	@$(LDPATH) valgrind --tool=callgrind ./nvrt # --track-origins=yes


clean:
	rm -rf obj/*
	rm -f sysdata/*.def

rmplots:
	rm -f plots/*.png
	rm -f plots/*.svg
	rm -f plots/*.mp4

rmdata:
	rm -rf outdata/*
