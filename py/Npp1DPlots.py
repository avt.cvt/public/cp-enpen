#!/usr/bin/env python3
from .Npp1D import Npp1D
from matplotlib.animation import FuncAnimation
import numpy.fft as fft
import matplotlib.cm as cm
import matplotlib.colors as colors
import scipy.constants
import scipy.signal as sg
import os
import numpy as np
from matplotlib import animation
import math
import sys
import json
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.figure import Figure
import pandas as pd
#mpl.use('Qt5Agg')

mpl.style.use('seaborn-white')
mpl.rcParams['savefig.dpi'] = 800
mpl.rcParams['figure.figsize'] = 7, 6
# mplt.rcParams['figure.figsize'] = 13.33, 7.5
mpl.style.use('seaborn-white')
plt.style.use(os.path.dirname(__file__) + '/cvt.mplstyle')


class Npp1DPlots:

    def __init__(self, dir):
        self.data = Npp1D(dir)

    def refresh(self):
        self.data.refresh()

    def drawZoneBounds(self,ax):
        for i,v in self.data.zones.items():
            ax.axvline(v['a'][0],color="black", linestyle="--",alpha=0.5)
            ax.axvline(v['b'][0],color="black", linestyle="--",alpha=0.5)

    def showFlux(self):
        t, i, v = self.data.getIV()
        f, (ax1, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [3, 1]})
        ax1.plot(t, i)
        ax1.set_ylabel('Current')
        ax2.plot(t,v,color='red')
        ax2.set_ylabel('Potential')
        plt.xlabel('Time')
        plt.tight_layout()

    def showConductivity(self):
        d = self.data
        f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
        writeState = np.ma.masked_where(d.timeEvolution['mode'] != 'write', d.timeEvolution['ptotal'])
        openState = np.ma.masked_where(d.timeEvolution['mode'] != 'open', d.timeEvolution['ptotal'])
        ax1.plot(d.times, writeState / d.timeEvolution['ptotal'][0], linestyle='dashed', color='gray')
        ax1.plot(d.times, openState / d.timeEvolution['ptotal'][0])
        ax2.plot(d.times, d.timeEvolution['pulse'], color='red')
        ax1.set_ylabel("$C / C_0$")
        ax2.set_ylabel("V")

    def showCV(self):
        t, i, v = self.data.getIV()
        plt.scatter(v,i)
        plt.xlabel('Potential (V)')
        plt.ylabel('Flux (A/$m^2$)')
        plt.tight_layout()

    def showConductivityOverPotential(self):
        conductivity = self.data.timeEvolution['ptotal'] / np.amax(self.data.timeEvolution['ptotal'])
        plt.scatter(self.data.timeEvolution['voltage'] * -1 / 40, conductivity)
        plt.ylabel('$C/C_{max}$')
        plt.xlabel('Potential (V)')