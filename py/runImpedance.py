#!/usr/bin/env python3
import numpy as np
import subprocess
import multiprocessing
import time
import sys
import os

program = sys.argv[1] if (len(sys.argv) > 1) else 'tsimpedance-run'
processes = int(sys.argv[2]) if (len(sys.argv) > 2 and int(sys.argv[2])>0) else 2

def work(freq):
    print('Simulating ' + str(freq) + 'Hz')
    subprocess.call(['make', program ,'FREQUENCY=' + str(freq)], stdout=subprocess.DEVNULL)
    return freq

pool = multiprocessing.Pool(processes=processes)

for f in pool.imap_unordered(work, np.flip(np.logspace(0,6,20))):
    print('Done with ' + str(f) + 'Hz')