#!/usr/bin/env python3
from py.NppPlot1D import NppPlot1D as nppp,plt
import sys
import glob
import re
import numpy as np
import math
import sys
import json
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import scipy

mpl.style.use('seaborn-white')
plt.style.use('py/cvt.mplstyle')

simulation = sys.argv[1] if len(sys.argv) > 1 else "SpeedStep"

simulations = glob.glob('outdata/' + simulation + '/V*')

threshold = 1.001

potential = []
time = []
energy = []

for s in simulations:
    pot = float(re.findall("\d+\.\d+", s)[0]) / 40
    d = nppp(s)
    ptotal = d.timeEvolution['ptotal']
    t = d.timeEvolution['time']
    potential.append(pot)
    time.append(threshold / (ptotal[-1] - ptotal[0]) * (t[-1] - t[0]))
    energy.append(d.timeEvolution['pulse'][0] / 40 * (ptotal[-1] - ptotal[0]) * time[-1] / (t[-1] - t[0])\
        * 1e-3 * scipy.constants.Avogadro * scipy.constants.elementary_charge)


fig, ax1 = plt.subplots()
plt.scatter(potential,time)

plt.ylabel('Write duration (s)')
plt.xlabel('Write potential (V)')

ax2 = plt.gca().twinx()
ax2.scatter(potential,energy,color='gray')
ax2.set_ylabel('Write energy (J/$m^2$)', color='gray')

# ax1.set_yscale('log')
# ax1.set_xscale('log')
# ax2.set_yscale('log')
# ax1.set_ylim(10**-1,10**3)
# ax2.set_ylim(10**-1,10**3)

plt.tight_layout()

df = pd.DataFrame({
    'time':time,
    'potential':potential,
    'energy':energy
    })

df.to_excel('plots/SpeedStepsPotential.xlsx')




fig, ax1 = plt.subplots()

simulations = glob.glob('outdata/' + simulation + '/Channel*')

channel = []
time = []
energy = []

for s in simulations:
    p = float(re.findall("\d+", s)[0])
    d = nppp(s)
    ptotal = d.timeEvolution['ptotal']
    t = d.timeEvolution['time']
    channel.append(p)
    time.append(threshold / (ptotal[-1] - ptotal[0]) * (t[-1] - t[0]))
    energy.append(d.timeEvolution['pulse'][0] / 40 * (ptotal[-1] - ptotal[0]) * time[-1] / (t[-1] - t[0])\
        * 1e-3 * scipy.constants.Avogadro * scipy.constants.elementary_charge)

plt.scatter(channel,time)

plt.ylabel('Write duration (s)')
plt.xlabel('Channel width (um)')

# ax2 = plt.gca().twinx()
# ax2.scatter(channel,energy,color='gray')
# ax2.set_ylabel('Write energy (J/$m^2$)', color='gray')

# ax1.set_yscale('log')
# ax1.set_xscale('log')
# ax2.set_yscale('log')

plt.tight_layout()

df = pd.DataFrame({
    'time':time,
    'channel':channel,
    'energy':energy
    })

df.to_excel('plots/SpeedStepsChannel.xlsx')

plt.show()