#!/usr/bin/env python3

from py.NppPlot1D import NppPlot1D as nppp,plt
import sys
import matplotlib as mpl
import os
import glob
import numpy as np
import re
import cmath
import scipy.signal as sg
from matplotlib.animation import FuncAnimation

mpl.style.use('seaborn-white')
plt.style.use('py/cvt.mplstyle')
mpl.rcParams['savefig.dpi'] = 800
mpl.rcParams['figure.figsize'] = 7,6

class ImpedancePlot():

	name = ''
	impedances = np.array([])
	frequencies = np.array([])
	anim = []

	def __init__(self,name):
		self.name = name
		self.loadSimulation()

	def loadSimulation(self):
		simulations = glob.glob(self.name + '*')
		for s in simulations:
			freq = float(re.findall("\d+\.\d+", s)[0])
			if np.isin(self.frequencies, [freq]).any(): continue
			try:
				d = nppp(s)
				if d.simulationComplete:              
					self.impedances = np.append(self.impedances, [d.impedance()])
					self.frequencies = np.append(self.frequencies,freq)
					print('Loaded: ' + s)
					print(d.impedance())
			except:
				'some error was caught'

	def refresh(self):
		self.loadSimulation()

	def nyquist(self):		
		fig = plt.figure('Nyquist')
		ax = plt.axes()

		plt.axis('equal')
		plt.ylabel("-Z\'\' ($\Omega\cdot m^2$)")
		plt.xlabel('Z\' ($\Omega\cdot m^2$)')

		sc = ax.scatter(self.impedances.real, -1*self.impedances.imag,c='black')#,c=self.frequencies,cmap='plasma',norm=mpl.colors.LogNorm())


		plt.tight_layout()
		ax.yaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=5))
		ax.xaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=5))
		ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(2))
		ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(2))

		# def update_live(frame):
		# 	self.refresh()
		# 	imp = self.impedances
		# 	sc.set_offsets(np.c_[imp.real,-1*imp.imag])

		# self.anim.append(FuncAnimation(fig, update_live, interval=100, blit=False))	

	def bode(self):
		
		fig,ax1 = plt.subplots(figsize=(7,6))

		plt.xlabel('Frequency ($s^{-1}$)')
		plt.ylabel('|Z| ($\Omega\cdot m^2$)')
		ax1.set_xscale('log')
		ax1.set_yscale('log')
		#ax1.tick_params(axis='y',which='both')

		ax2 = ax1.twinx()
		ax2.set_xscale('log')
		ax2.set_ylabel('-Phase (°)')	
		#ax2.tick_params(axis='y')

		labs, = ax1.plot(self.frequencies,np.abs(self.impedances),'k.', markersize=16)
		lang, = ax2.plot(self.frequencies,-1*np.angle(self.impedances, deg=True),'ks',color='red')

		plt.tight_layout()
		# ax2.yaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=5))
		# ax2.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(2))

		# def update_live(frame):
		# 	self.refresh()
		# 	imp = self.impedances
		# 	f = self.frequencies
		# 	labs.set_data(f,np.abs(imp))
		# 	lang.set_data(f,np.angle(imp,deg=True))
		# 	ax1.relim()
		# 	ax2.relim()
		# 	ax1.autoscale_view()
		# 	ax2.autoscale_view()
		# 	# if len(self.frequencies) > 1:
		# 	# 	ax1.set_xlim(f.min(),f.max())
		# 	# 	ax1.set_ylim(np.abs(imp).min(),np.abs(imp).max())
		# 	# 	ax2.set_ylim(np.angle(imp,deg=True).min(),np.angle(imp,deg=True).max())

		# self.anim.append(FuncAnimation(fig, update_live, interval=100, blit=False))	
