#!/usr/bin/env python3
from matplotlib.animation import FuncAnimation
import numpy.fft as fft
import matplotlib.cm as cm
import matplotlib.colors as colors
import scipy.constants
import scipy.signal as sg
import os
import numpy as np
from matplotlib import animation
import math
import sys
import json
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.figure import Figure
import pandas as pd
#mpl.use('Qt5Agg')

mpl.style.use('seaborn-white')
mpl.rcParams['savefig.dpi'] = 800
mpl.rcParams['figure.figsize'] = 7, 6
# mplt.rcParams['figure.figsize'] = 13.33, 7.5


class NppPlot1D:

    T = 298.15
    anim = []
    states = []
    fluxes = []
    charges = []
    times = []
    simulationComplete = True

    def __init__(self, dir):
        self.dir = dir

        self.loadIndex()

        self.volumes = np.loadtxt(dir + "/volumes", delimiter=",")
        self.faces = np.loadtxt(dir + "/faces", delimiter=",")

    def refresh(self):
        if not self.simulationComplete:
            self.loadIndex()

    def loadIndex(self):

        ij = open(self.dir + "/index.json", "r")
        try:
            self.index = json.load(ij)
        except:
            try:
                ij.seek(0)
                self.index = json.loads(ij.read() + '}}')
                self.simulationComplete = False
            except:
                return

        self.nv = self.index["nv"]
        self.names = self.index["species"]
        self.count = len(self.index["indices"])
        self.charges = [float(c) for c in self.index["charges"]]
        self.zones = self.index["zones"]

        self.states = [0]*self.count
        self.fluxes = [0]*self.count

        self.times = np.array([x['time'] for x in self.index['indices'].values()])

        self.timeEvolution = {
            'time': np.array([x['time'] for x in self.index['indices'].values()]),
            'fluxes': np.array([x['fluxes'] for x in self.index['indices'].values()]),
            'voltage': np.array([x['voltage'] for x in self.index['indices'].values()]),
            'displacementCurrent': np.array([x['displacementCurrent'] for x in self.index['indices'].values()]),
            'ptotal': np.array([x['ptotal'] for x in self.index['indices'].values()]),
            'pulse': np.array([x['pulse'] for x in self.index['indices'].values()]),
            'mode': np.array([x['mode'] for x in self.index['indices'].values()])
        }

    def getstate(self, index):
        if(not isinstance(self.states[index], (np.ndarray))):
            self.states[index] = np.genfromtxt(
                self.dir + "/state/" + str(index), delimiter=",")
            #print('Loading state ' + str(index) + " ...")

        return self.states[index]

    def getflux(self, index):
        if(not isinstance(self.fluxes[index], (np.ndarray))):
            self.fluxes[index] = np.genfromtxt(
                self.dir + "/flux/" + str(index), delimiter=",")
            #print('Loading flux ' + str(index) + " ...")

        return self.fluxes[index]

    def impedance(self):
        
        def fftransform(signal):
            nfft = signal.size
            n = np.floor(0.5 * nfft)
            n = int(n)
            s_ft = fft.fft(signal)
            idx = np.argmax(np.abs(s_ft[1:n]))
            z = s_ft[idx + 1] / n

            return z

        t, i, v = self.fromPTotal()

        V0 = fftransform(v)

        I = fftransform(i)

        imp = V0/I

        return imp

    def plot_state(self, index, species=[]):
        fig_spec = plt.figure()
        plots = []
        ax = plt.axes()
        state = self.getstate(index)
        if(not species):
            species = range(0, self.nv-2)
        for i in species:
            plot, = ax.plot(self.volumes[:, 0], state[:, i])
            plots.append(plot)
            plot.set_label(self.names[i])
        plt.legend()
        plt.ylabel("Concentration")
        plt.xlabel('Space [$\mu$m]')

    def plotStates(self, species, times=[0]):
        fig_spec = plt.figure()
        for i in times:
            state = self.getstate(i)
            plt.plot(x, state[:, species], color='black')
        
        self.drawZoneBounds(plt.gca())
        plt.xlabel('x ($\mu$m)')

    def plot_evolution(self, species, times=[0]):

        blue = cm.get_cmap('Blues')
        cNorm = colors.Normalize(vmin=-1, vmax=len(times))
        scalarMap = cm.ScalarMappable(norm=cNorm, cmap=blue)
        scalarMap.set_array(range(0, self.count))
        fig_spec = plt.figure(figsize=(13.33, 7.5))
        plots = []
        ax = plt.axes()
        for i in times:
            state = self.getstate(i)
            colorVal = scalarMap.to_rgba(i)
            plot, = ax.plot(self.volumes[:, 0],
                            state[:, species], color=colorVal)
            plots.append(plot)
        cbar = plt.colorbar(scalarMap, ticks=[0, len(times)])
        cbar.ax.set_yticklabels(['0', len(times)])
        cbar.set_label('Timestep')
        plt.xlabel('Space [$\mu$m]')
        plt.ylabel(self.names[species].upper())

    def flux_evolution(self, species, ax, step):# times=[0]):  # work on runtime
        # for i in range(0,self.count-1):
        #     self.getflux(i)
        alpha = self.getflux(0)
        beta =self.getflux(step)
        omega = self.getflux(self.count-1)
        
        plot, = ax.plot(self.faces[:,0], beta[:,species], '.', markersize=3.6)
        # plot_end = ax.plot(self.faces[:,0], omega[:,species], '.', markersize=3.6)
        # plot_0 = ax.plot(self.faces[:,0], alpha[:,species], '.', markersize=3.6)
        
        # ax.legend(['animated', '$t_{end}$', '$t_0$'])
        ax.set_title('State')
        ax.set_xlabel('Space [$\mu$m]')
        ax.set_ylabel(self.names[species].upper())
        
        return(plot)
        
    def flux_update(self, plot, species, step):
        flux = self.getflux(step)
        plot.set_data(self.faces[:,0], flux[:,species])
        # blue = cm.get_cmap('Blues')
        # cNorm = colors.Normalize(vmin=-1, vmax=len(times))
        # scalarMap = cm.ScalarMappable(norm=cNorm, cmap=blue)
        # scalarMap.set_array(range(0, self.count))
        # fig_spec = plt.figure(figsize=(13.33, 7.5))
        # plots = []
        # ax = plt.axes()
        # for i in times:
        #     state = self.getflux(i)
        #     colorVal = scalarMap.to_rgba(i)
        #     # first and last flux are not calculated at the moment
        #     plot, = ax.plot(self.faces[:, 0],
        #                     state[:, species], '.', color=colorVal)
        #     plots.append(plot)
        #     plot.set_label(self.names[species] + ' ' + str(i))
        # #cbar = plt.colorbar(scalarMap, ticks=[0, len(times)])
        # #cbar.ax.set_yticklabels(['0', len(times)])
        # #cbar.set_label('time in [sec]')
        # plt.xlabel('Space [$\mu$m]')
        # plt.ylabel(self.names[species].upper())

    def flux_from_total(self):
        t, i, v = self.fromPTotal()
        plt.plot(t, i)
        plt.ylabel('Current')
        plt.gca().twinx().plot(t,v,color='red')
        plt.gca().set_ylabel('Potential')

    def CVFromTotal(self):
        t, i, v = self.fromPTotal()
        plt.xlabel('Potential (V)')
        plt.ylabel('Flux (A/$m^2$)')
        plt.scatter(v,i)
        plt.tight_layout()

    def exportIV(self, file='plots/output.csv'):
        t, i, v = self.fromPTotal()
        df = pd.DataFrame({'Time': t, 'Potential (V)': v, 'Flux (A/m2)': i})
        df.to_csv(file)

    def conductivity(self):
        f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
        writeState = np.ma.masked_where(self.timeEvolution['mode'] != 'write', self.timeEvolution['ptotal'])
        openState = np.ma.masked_where(self.timeEvolution['mode'] != 'open', self.timeEvolution['ptotal'])
        ax1.plot(self.times, writeState / self.timeEvolution['ptotal'][0], linestyle='dashed', color='gray')
        ax1.plot(self.times, openState / self.timeEvolution['ptotal'][0])
        ax2.plot(self.times, self.timeEvolution['pulse'], color='red')
        ax1.set_ylabel("$C / C_0$")
        ax2.set_ylabel("V")

        df = pd.DataFrame({
            'time':self.times,
            'mode':self.timeEvolution['mode'],
            'ptotal':self.timeEvolution['ptotal'],
            'pulse':self.timeEvolution['pulse'],
            'voltage':self.timeEvolution['voltage']
            })

        df.to_excel('plots/conductivity.xlsx')



    def conductivityVsPotential(self):
        openState = np.ma.masked_where(self.timeEvolution['mode'] != 'open', self.timeEvolution['ptotal'])
        plt.scatter(self.timeEvolution['voltage'], openState / self.timeEvolution['ptotal'][0])

    def PPCorr(self):
        t = np.array(self.times)
        pt = self.timeEvolution['ptotal']

        dt = []
        dC = []

        for i in range(1, len(self.times)-1,2):
            dt.append(t[i])
            dC.append((pt[i+1] - pt[i]) / pt[0])

        plt.plot(dt,dC)

        df = pd.DataFrame({
            'dt':dt,
            'dC':dC
            })

        df.to_excel('plots/ppCorr.xlsx')

    def fromPTotal(self):
        t = np.array(self.times)
        pt = self.timeEvolution['ptotal']
        v = self.timeEvolution['voltage'] / 40
        dpdt = np.diff(pt) / np.diff(t)
        t2 = (t[:-1] + t[1:]) / 2
        v2 = (v[:-1] + v[1:]) / 2
        i = dpdt * 1e-3 * scipy.constants.Avogadro * scipy.constants.elementary_charge
        return t2, i, v2

    def flux_over_time(self, spec=0):
        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()

        def potential():
            return self.timeEvolution['voltage'] / 40  # in volt

        def flux():
            fluxes = self.timeEvolution['fluxes'][:,spec].copy()  # are in mol/s/m2
            # convert to ampere/m2
            fluxes *= scipy.constants.Avogadro * scipy.constants.elementary_charge
            displacementCurrent = self.timeEvolution['displacementCurrent'].copy()
            displacementCurrent = displacementCurrent*scipy.constants.Avogadro * \
                scipy.constants.elementary_charge

            return fluxes + displacementCurrent

        fl1, = ax1.plot(self.times, flux(), '.', color='tab:blue')
        ax1.set_xlabel('Time (s)')
        ax1.set_ylabel('Flux (A/m2)', color='tab:blue')

        ax1.tick_params(axis='y', labelcolor='tab:blue')
        ax2.set_ylabel('Volts (V)', color='tab:red')
        ax2.tick_params(axis='y', labelcolor='tab:red')

        vl1, = ax2.plot(self.times, potential(), '.', color='tab:red')
        #ax1.set_xlim(0, 10)
        #ax1.set_ylim(-220, 220)
        #ax2.set_ylim(-1.2, 1.2)

        def update_live(frame):
            self.refresh()
            fl1.set_data(self.times, flux())
            vl1.set_data(self.times, potential())

        self.anim.append(FuncAnimation(
            fig, update_live, interval=1000, blit=False))

    def flux_over_time1(self, spec=0):
        fig, ax1 = plt.subplots()

        def flux():
            fluxes = self.timeEvolution['fluxes'][:,spec].copy()  # are in mol/s/m2
            # convert to ampere/m2
            fluxes *= scipy.constants.Avogadro * scipy.constants.elementary_charge

            return fluxes

        fl1, = ax1.plot(self.times, flux(),'.', c="black")
        ax1.set_xlabel('Time (s)')
        ax1.set_ylabel('Current Flux (A/$m^2$)')

        ax1.yaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=5))
        ax1.xaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=5))
        ax1.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(2))
        ax1.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(2))

        def update_live(frame):
            self.refresh()
            fl1.set_data(self.times, flux())

        self.anim.append(FuncAnimation(
            fig, update_live, interval=1000, blit=False))

    def voltage_over_time(self, spec=-1):
        fig, ax1 = plt.subplots()

        def potential():
            return self.timeEvolution['voltage'] / 40  # in volt


        fl1, = ax1.plot(self.times, potential(), '.', color='tab:red')
        ax1.set_xlabel('Time (s)')
        ax1.set_ylabel('Voltage (V)', color='tab:red')

        ax1.tick_params(axis='y', labelcolor='tab:red')

        def update_live(frame):
            self.refresh()
            fl1.set_data(self.times, potential())

        self.anim.append(FuncAnimation(
            fig, update_live, interval=1000, blit=False))
    
    def cyclic_voltammetry(self, spec=-1):
        fig, ax = plt.subplots(figsize=(7.5, 6))

        def flux():
            fluxes = self.timeEvolution['fluxes'][:, 0].copy()
            fluxes *= scipy.constants.Avogadro * scipy.constants.elementary_charge

            return fluxes

        def potential():
            return self.timeEvolution['voltage'] / 40

        sc = ax.scatter(potential(), flux(), c="black")
        plt.xlabel('Potential (V)')
        plt.ylabel('Flux (A/$m^2$)')
        ax.yaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=5))
        ax.xaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=5))
        ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(2))
        ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(2))


        def update_live(frame):
            self.refresh()
            f = flux()
            p = potential()
            sc.set_offsets(np.c_[p, f])
            # ax.set_ylim(f.min(), f.max())
            # ax.set_xlim(p.min(), p.max())

        self.anim.append(FuncAnimation(
            fig, update_live, interval=100, blit=False))

    def plot_phi(self, index):
        fig_phi = plt.figure(figsize=(13.33, 7.5))
        ax = plt.axes()
        state = self.getstate(index)
        phi, = plt.plot(self.volumes[:, 0], state[:, self.nv-1])
        plt.legend([phi], [self.names[self.nv-1]])
        plt.xlabel('Space [$\mu$m]')
        plt.ylabel('Potential')
        plt.tight_layout()

    def plot_E(self, index):
        fig_E = plt.figure()
        state = self.getstate(index)
        y = state[:, self.nv-1]
        x = self.volumes[:, 0]
        dy = np.zeros(len(y), np.float)
        dy[0:-1] = -1*np.diff(y)/np.diff(x)
        dy[-1] = -1*(y[-1] - y[-2])/(x[-1] - x[-2])

        E, = plt.plot(x, dy)
        plt.xlabel('Space [$\mu$m]')
        plt.ylabel('E Field')

    def animate_s(self, plot, species, step):
        state = self.getstate(step)
        plot.set_data(self.volumes[:, 0], state[:, species])

    def animate_state_data(self, species, ax, first, last, running):
        # Cache states for better performance
        for i in range(0, self.count-1):
            self.getstate(i)
            
        anim_ax = plt.axes()
        alpha = self.getstate(0)
        omega = self.getstate(self.count-1)
        plot, = ax.plot(
            self.volumes[:, 0], omega[:, species], '.', markersize=3.6)
        plot_end, = ax.plot(
            self.volumes[:, 0], omega[:, species], '.', markersize=3.6)
        plot_0, = ax.plot(
            self.volumes[:, 0], alpha[:, species], '.', markersize=3.6)
        plt.legend(['animated', '$t_{end}$', '$t_0$'])
        plt.title('State')
        plt.xlabel('Space [$\mu$m]')
        plt.ylabel(self.names[species])
        return(plot_end, plot_0, plot, self.count-1)

    def drawZoneBounds(self,ax):
        for i,v in self.zones.items():
            ax.axvline(v['a'][0],color="black", linestyle="--",alpha=0.5)
            ax.axvline(v['b'][0],color="black", linestyle="--",alpha=0.5)
    
    def animate_state_GUI(self, species, ax,step):
        # Cache states for better performance
        # for i in range(0, self.count-1):
        #     self.getstate(i)
        alpha = self.getstate(0)
        beta = self.getstate(step)
        omega = self.getstate(self.count-1)
        
        plot, = ax.plot(
            self.volumes[:, 0], beta[:, species], '.', markersize=3.6)
        # plot_end, = ax.plot(
        #     self.volumes[:, 0], omega[:, species], '.', markersize=3.6)
        # plot_0, = ax.plot(
        #     self.volumes[:, 0], alpha[:, species], '.', markersize=3.6)
        # ax.legend(['animated', '$t_{end}$', '$t_0$'])
        ax.set_title('State')
        ax.set_xlabel('Space [$\mu$m]')
        ax.set_ylabel(self.names[species].upper())

        self.drawZoneBounds(ax)
        
        return(plot)#, plot_0, plot_end)

    def get_steps(self):
        return(self.count-1)

    def animate_f(self, plot, species, frame):
        flux = self.getflux(frame)
        plot.set_data(self.faces[:, 0], flux[:, species])

    def flux_over_voltage(self,spec=-1):

        def potential():
            return self.timeEvolution['voltage'] / 40 # in volt

        def flux():
            fluxes = self.timeEvolution['fluxes'][:,spec].copy() # are in mol/s/m2
            # convert to ampere/m2
            fluxes *= scipy.constants.Avogadro * scipy.constants.elementary_charge
            displacementCurrent = self.timeEvolution['displacementCurrent'].copy()
            displacementCurrent *= scipy.constants.Avogadro * scipy.constants.elementary_charge

            return fluxes + displacementCurrent

        plt.plot(flux(),potential(),'.',color='tab:blue')
        plt.xlabel('Flux [A/m2]', color='tab:blue')
        plt.ylabel('Voltage [V]')

    def animate_flux(self, species):
        # Cache states for better performance
        for i in range(0, self.count-1):
            self.getflux(i)

        anim_fig = plt.figure(figsize=(13.33, 7.5))
        anim_ax = plt.axes()
        alpha = self.getflux(0)
        omega = self.getflux(self.count-1)
        plot, = anim_ax.plot(
            self.faces[:, 0], omega[:, species], '.', markersize=5)
        plot_end, = anim_ax.plot(
            self.faces[:, 0], omega[:, species], '.', markersize=5)
        plot_0, = anim_ax.plot(
            self.faces[:, 0], alpha[:, species], '.', markersize=5)
        plt.legend(['animated', '$t_{end}$', '$t_0$'])
        plt.title('Flux')
        plt.xlabel('Space [$\mu$m]')
        plt.ylabel(self.names[species])
        self.anim.append(FuncAnimation(anim_fig, lambda frame: self.animate_f(plot, species, frame),
                                       frames=self.count, interval=20, blit=False))

    def create_txt(self, file='plots/charging.txt', spec=-1):
        #creates a .txt file with time, voltages and fluxes
        
        times, fluxes , voltages = self.fromPTotal()
        
        
        #f= open("charging.txt","w+")
        with open(file, 'w+') as f:
            f.write('Times:\n')
            for item in times:
                f.write("%s\n" % item)
            f.write('Fluxes:\n')
            for item in fluxes:
                f.write("%s\n" % item)
            f.write('Voltages:\n')
            for item in voltages:
                f.write("%s\n" % item)
        f.close()
        
