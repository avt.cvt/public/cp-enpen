#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif
#include <petscsys.h>

#include "src/ParameterStore.h"
#include "src/Application.h"
#include "src/Models/TybrandtNPP.h"
#include "src/Models/TybrandtShuttleNPP.h"
#include "src/Mesh/Mesh1D.h"
#include "src/ResultWriter/ResultWriter.h"
#include "src/ResultWriter/ResultLoader.h"
#include <string>
#include <map>
#include "math.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <list>

#include "src/Models/BVReaction.h"

int main(int argc, char **argv)
{
    PetscErrorCode ierr;
    ierr = PetscInitialize(&argc, &argv, NULL, NULL);
    CHKERRXX(ierr);

    /* Set up simulation */
    ParameterStore p("sysdata/tybrandt-shuttle-dissociation.def");

    auto nm = p.nameMap;

    TybrandtShuttleNPP model;

    Application app(&p, (Model *)&model);
    unsigned int n = app.nv;

    double k_O2pH2O2 = 1;//1e2;

    // O2 + 2H+ + 2e- <-> H2O2
    BVReaction shuttle(p.num_species,nm["H2O2"],nm["O2"], 1e-3/k_O2pH2O2, 1e-3);
    shuttle.n = 2; // two electron reaction
    shuttle.mu[nm["OH-"]] = -2; // coupled with water dissociation
    shuttle.k_0 = 3e-12;
    shuttle.k_0 = 1.3e-12; //Charging comparison
    // shuttle.E0 = 2.13117658363210438;

    // todo: test why no effect of co2e

    // Mesh1D mesh("meshdata/CP_EL_CP-200-1D.msh", n);
    Mesh1D mesh("meshdata/CP_EL_CP-300-1D.msh", n);

    double cel = 1e-1;
    double cH = 1e-7;

    double cO = 1e-3 * 0.1;
    double cH2O2 = cO/k_O2pH2O2;

    double initial[] = {0, cel, cel, cH, cH, cO, cH2O2, 0, 0}; // Values normalized to M
    mesh.set_state(&initial[0], n);

    double BModLeft = 0, BModRight = 0;

    // // self discharge experiment
    BModLeft = -0.38;
    BModRight = BModLeft;


    // // for corr conc conductivity
    // BModLeft = -0.22;
    // BModRight = BModLeft;

    // // No PEI
    // BModLeft = 0;

    // // 50% PEI
    // BModLeft = +0.17; // PEI fit

    // 80% PEI
    // BModLeft = +0.73;

    // electrolyte fit, no pei, but larger stability range?
    // BModLeft = +0; // Elec, air
    // BModLeft = +0.3; // Elec, N2

    // note that they should be symmetric
    // BModLeft = -0.5; // Naf, N2
    // BModLeft = -0.84; // Naf, Air

    Zone *cpleft_zone = mesh.getZone("CPLEFT");
    cpleft_zone->charge = -1;
    cpleft_zone->alpha = 0.25;
    cpleft_zone->Bmod = p.normalizeBMod(BModLeft);
    cpleft_zone->applyToVolumes();

    Zone *elleft_zone = mesh.getZone("ELLEFT");
    elleft_zone->stateIsActive[nm["PEDOT+"]] = false;
    elleft_zone->stateIsActive[nm["PHI_CP"]] = false;
    // elleft_zone->alpha = 5e-2;
    elleft_zone->applyToVolumes();

    Zone *elright_zone = mesh.getZone("ELRIGHT");
    elright_zone->stateIsActive[nm["PEDOT+"]] = false;
    elright_zone->stateIsActive[nm["PHI_CP"]] = false;
    // elright_zone->alpha = 5e-2;
    elright_zone->applyToVolumes();

    Zone *cpright_zone = mesh.getZone("CPRIGHT");
    cpright_zone->charge = -1;
    cpright_zone->alpha = 0.25;
    cpright_zone->Bmod = p.normalizeBMod(BModRight);
    cpright_zone->applyToVolumes();

    mesh.initialize();
    app.setMesh((Mesh *)&mesh);

    mesh.set_boundary_state(FACETYPE_BOUNDARY_RIGHT, &initial[0], n);
    mesh.set_boundary_state(FACETYPE_BOUNDARY_LEFT, &initial[0], n);

    for (auto& f : elleft_zone->getRightBoundary()) {
        f->setBoundaryState(&initial[0]);
    }
    for (auto& f : cpleft_zone->getRightBoundary()) {
        f->setBoundaryState(&initial[0]);
    }
    for (auto& f : cpright_zone->getRightBoundary()) {
        f->setBoundaryState(&initial[0]);
    }

    ResultWriter rw("outdata/shuttle_init", &app);

    int bcond_cp[] = {3, 2, 2, 2, 2, 2, 2, 0, 2};
    int bcond_el[] = {2, 0, 0, 0, 0, 0, 0, 2, 0};
    mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_cp[0]);
    mesh.setBoundaryCondition(cpright_zone->getRightBoundary(), &bcond_cp[0]);

    mesh.setBoundaryCondition(elleft_zone->getRightBoundary(), &bcond_el[0]);

    app.pseudoTransient();
    cpright_zone->bvreaction = &shuttle;
    cpright_zone->updateVolumeProperties();
    cpleft_zone->bvreaction = &shuttle;
    cpleft_zone->updateVolumeProperties();

    int bcond_none[] = {-1, -1, -1, -1, -1, -1, -1, -1, -1};
    mesh.setBoundaryCondition(elleft_zone->getRightBoundary(), &bcond_none[0]);

    app.rw = &rw;
    app.transient(1e0,1e-3);
    app.transient(1e3,5e-1);
    rw.close();
    app.rw = NULL;

    std::cout << app.getOCP(n-2);

    ResultWriter rwinitial("outdata/InitialState", &app);
    rwinitial.write_state(0);
    rwinitial.write_state(0);
    rwinitial.close();


    // Self discharge
    auto SD = [&] (double dV, double dt, double step = 1e-1, double programtime = 50, double programstep = 1e-1) {   
        double bias = dV;
        int bcond_opencircuit[] = {2, 2, 2, 2, 2, 2, 2, 2, 2};
        mesh.modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, n-2);
        app.pulse = dV;
        app.mode = "write";
        app.transient(programtime, 1e-1);
        mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_opencircuit[0]);
        app.pulse = 0;
        app.mode = "open";
        app.transient(dt, step);
        mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_cp[0]);   
    };

    // // SelfDischarge short time
    // ResultWriter rwsd("outdata/SelfDischargeShort", &app);
    // app.rw = &rwsd;
    // app.transient(1e1, 1e-1);
    // SD(-20,600,1e-1,170,5e-2);
    // SD(-10,600,1e-1,170,5e-2);
    // SD(0,600,1e-1,170,5e-2);
    // SD(10,600,1e-1,170,5e-2);
    // SD(20,600,1e-1,170,5e-2);
    // // SD(0,600,1e-1, 10);
    // // SD(-10,600,1e-1, 10);
    // // SD(-20,600,1e-1, 10);
    // rwsd.close(); 

    // // SelfDischarge long
    // ResultWriter rwsdn("outdata/SelfDischargeLong", &app);
    // app.rw = &rwsdn;
    // rwsdn.fullWrite=false;
    // SD(20,1e4,1e-1,50,5e-2);
    // rwsdn.close(); 

    // // ChargingComparison
    // ResultWriter rwsdn("outdata/ChargingComparison", &app);
    // app.rw = &rwsdn;
    // rwsdn.fullWrite=false;
    // SD(0,80,1e-1,10);
    // // SD(-20,600,1e-1,170,5e-1);
    // SD(-20,1e4,1e-1,170,5e-2);
    // rwsdn.close(); 

    // ChargingComparison Short program
    ResultWriter rwsdn("outdata/ChargingComparison", &app);
    app.rw = &rwsdn;
    rwsdn.fullWrite=false;
    SD(0,80,1e-1,10);
    // SD(-20,600,1e-1,170,5e-1);
    SD(-20,1e4,1e-1,1,5e-2);
    rwsdn.close(); 

    // // ChargingComparison other side
    // ResultWriter rwsdn("outdata/ChargingComparison", &app);
    // app.rw = &rwsdn;
    // rwsdn.fullWrite=false;
    // SD(0,80,1e-1,10);
    // SD(20,1e4,1e-1,170,5e-2);
    // rwsdn.close(); 
    
/*
    // Conductivity over Potential
    auto SteadyState = [&] (double bias, ResultWriter &rw, double duration = 100, double dt = 1e-1) {
        mesh.modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, n-2);
        app.transient(duration, dt);
        rw.write_state(bias);
    };

    ResultWriter rwcov("outdata/ConductivityOverPotential", &app);
    // SteadyState(-20, rwcov, 200);
    // for(int i = -5; i<=5; i++){
    //     SteadyState(((double)i)/10 * 40, rwcov);
    // }

    // // No PEI
    // for(int i = 0; i<=10; i++){
    //     SteadyState(((double)i)/10 * 40 * -1, rwcov);
    // }

    // // 50% PEI
    // for(int i = -4; i<=5; i++){
    //     SteadyState(((double)i)/10 * 40 * -1, rwcov,10);
    // }

    // // 80% PEI
    // for(int i = -5; i<=1; i++){
    //     SteadyState(((double)i)/10 * 40 * -1, rwcov, 10, 1e-2);
    // }

    // // Elec
    // for(int i = -4; i<=5; i++){
    //     SteadyState(((double)i)/10 * 40 * -1, rwcov,10);
    // }
    // // Naf
    // for(int i = -5; i<=5; i++){
    //     SteadyState(((double)i)/10 * 40 * -1, rwcov,10);
    // }

    // carrier concentration conductivity correlation
    for(int i = -5; i<=5; i++){
        SteadyState(((double)i)/10 * 40 * -1, rwcov);
    }


    rwcov.close();
*/
/*
    auto StabilityTest = [&] (double bias, ResultWriter &rw){
        app.pulse = bias;
        mesh.modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, n-2);
        app.transient(2e1, 1e-1);
        int bcond_opencircuit[] = {2, 2, 2, 2, 2, 2, 2, 2, 2};
        mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_opencircuit[0]);
        rw.write_state(bias);
        app.transient(5*60, 1e-1);
        rw.write_state(bias);
        mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_cp[0]); 
    };

    ResultWriter rwst("outdata/StabilityTest", &app);
    // Full range
    // StabilityTest(0,rwst);
    // for(int i = 0; i<=10; i++){
    //     StabilityTest(((double)i)/10 * 40 * -1, rwst);
    // }

    // no PEI inverted
    for(int i = -10; i<=0; i++){
        StabilityTest(((double)i)/10 * 40, rwst);
    }
    // // No PEI
    // for(int i = 0; i<=10; i++){
    //     StabilityTest(((double)i)/10 * 40 * -1, rwst);
    // }

    // // High PEI only one side
    // for(int i = -5; i<=0; i++){
    //     StabilityTest(((double)i)/10 * 40 * -1, rwst);
    // }

    // // symmetric
    // for(int i = -5; i<=5; i++){
    //     StabilityTest(((double)i)/10 * 40, rwst);
    // }

    //  // default 50 PEI
    // for(int i = -4; i<=5; i++){
    //     StabilityTest(-1 * ((double)i)/10 * 40, rwst);
    // }

    rwst.close();
*/

/*
    /// vDB Pulses
    auto vDBPulse = [&] (double dV, double dtprogram, double dtmeasure, double stepprogram = 1e-1, double stepmeasure = 1e-1) {   
        int bcond_opencircuit[] = {2, 2, 2, 2, 2, 2, 2, 2, 2};
        double bias = app.getOCP(n-2) + dV;
        mesh.modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, n-2);
        app.pulse = dV;
        app.mode = "write";
        app.transient(dtprogram, stepprogram);
        mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_opencircuit[0]);
        app.pulse = 0;
        app.mode = "open";
        app.transient(dtmeasure, stepmeasure);
        mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_cp[0]);   
    };

    ResultWriter rwvdb("outdata/vDBPulse", &app);
    rwvdb.fullWrite = false;
    app.rw = &rwvdb;
    for(int i = 0; i<75; i++)
        vDBPulse(1e-2 * 40, 1e-1,2e1,1e-2,1e-1);
    for(int i = 0; i<150; i++)
        vDBPulse(-1 * 1e-2 * 40, 1e-1,2e1,1e-2,1e-1);
    for(int i = 0; i<150; i++)
        vDBPulse(1 * 1e-2 * 40, 1e-1,2e1,1e-2,1e-1);
    for(int i = 0; i<75; i++)
        vDBPulse(-1 * 1e-2 * 40, 1e-1,2e1,1e-2,1e-1);
*/


    /* Finish and exit */
    app.destroy();
    return PetscFinalize();
}
