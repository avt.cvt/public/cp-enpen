#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif
#include <petscsys.h>

#include "src/ParameterStore.h"
#include "src/Application.h"
#include "src/Models/TybrandtNPP.h"
#include "src/Models/TybrandtShuttleNPP.h"
#include "src/Mesh/Mesh1D.h"
#include "src/ResultWriter/ResultWriter.h"
#include "src/ResultWriter/ResultLoader.h"
#include <string>
#include <map>
#include "math.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <list>

#include "src/Models/BVReaction.h"

int main(int argc, char **argv)
{
    PetscErrorCode ierr;
    ierr = PetscInitialize(&argc, &argv, NULL, NULL);
    CHKERRXX(ierr);

    /* Set up simulation */
    ParameterStore p("sysdata/tybrandt-shuttle-dissociation.def");

    auto nm = p.nameMap;

    TybrandtShuttleNPP model;

    Application app(&p, (Model *)&model);
    unsigned int n = app.nv;

    double k_O2pH2O2 = 1; // 1e2;

    // O2 + 2H+ + 2e- <-> H2O2
    BVReaction shuttle(p.num_species, nm["H2O2"], nm["O2"], 1e-3 / k_O2pH2O2, 1e-3);
    shuttle.n = 2;              // two electron reaction
    shuttle.mu[nm["OH-"]] = -2; // coupled with water dissociation
    shuttle.k_0 = 3e-12;
    shuttle.k_0 = 1.3e-12; // Charging comparison
    // shuttle.E0 = 2.13117658363210438;

    // shuttle.k_0 = 1e-9; // mod for faster self discharge

    // todo: test why no effect of co2e

    // Mesh1D mesh("meshdata/CP_EL_CP-200-1D.msh", n);
    Mesh1D mesh("meshdata/CP_EL_CP-300-1D.msh", n);

    double cel = 1e-1;
    double cH = 1e-7;

    double cO = 1e-3;
    double cH2O2 = cO / k_O2pH2O2;

    double initial[] = {0, cel, cel, cH, cH, cO, cH2O2, 0, 0}; // Values normalized to M
    mesh.set_state(&initial[0], n);

    double BModLeft = 0, BModRight = 0;

    Zone *cpleft_zone = mesh.getZone("CPLEFT");
    cpleft_zone->charge = -1;
    cpleft_zone->alpha = 0.25;
    cpleft_zone->Bmod = p.normalizeBMod(BModLeft);
    cpleft_zone->applyToVolumes();

    Zone *elleft_zone = mesh.getZone("ELLEFT");
    elleft_zone->stateIsActive[nm["PEDOT+"]] = false;
    elleft_zone->stateIsActive[nm["PHI_CP"]] = false;
    // elleft_zone->alpha = 5e-2;
    elleft_zone->applyToVolumes();

    Zone *elright_zone = mesh.getZone("ELRIGHT");
    elright_zone->stateIsActive[nm["PEDOT+"]] = false;
    elright_zone->stateIsActive[nm["PHI_CP"]] = false;
    // elright_zone->alpha = 5e-2;
    elright_zone->applyToVolumes();

    Zone *cpright_zone = mesh.getZone("CPRIGHT");
    cpright_zone->charge = -1;
    cpright_zone->alpha = 0.25;
    cpright_zone->Bmod = p.normalizeBMod(BModRight);
    cpright_zone->applyToVolumes();

    mesh.initialize();
    app.setMesh((Mesh *)&mesh);

    mesh.set_boundary_state(FACETYPE_BOUNDARY_RIGHT, &initial[0], n);
    mesh.set_boundary_state(FACETYPE_BOUNDARY_LEFT, &initial[0], n);

    for (auto &f : elleft_zone->getRightBoundary())
    {
        f->setBoundaryState(&initial[0]);
    }
    for (auto &f : cpleft_zone->getRightBoundary())
    {
        f->setBoundaryState(&initial[0]);
    }
    for (auto &f : cpright_zone->getRightBoundary())
    {
        f->setBoundaryState(&initial[0]);
    }

    int bcond_cp[] = {3, 2, 2, 2, 2, 2, 2, 0, 2};
    int bcond_el[] = {2, 0, 0, 0, 0, 0, 0, 2, 0};
    mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_cp[0]);
    mesh.setBoundaryCondition(cpright_zone->getRightBoundary(), &bcond_cp[0]);

    // Receive simulation parameters
    std::string name = (argc > 1) ? std::string(argv[1]) : "ts-api-default";
    double simtime = (argc > 2) ? atof(argv[2]) : 1;
    bool ocp = (argc > 3) ? false : true;
    double bias = (argc > 3) ? atof(argv[3]) : 0;

    ResultLoader rl(&app);
    if (rl.load("outdata/" + name + "/state/0"))
    {
        PetscPrintf(PETSC_COMM_WORLD, "Loading results...\n");

        cpright_zone->bvreaction = &shuttle;
        cpright_zone->updateVolumeProperties();
        cpleft_zone->bvreaction = &shuttle;
        cpleft_zone->updateVolumeProperties();
    }
    else
    {

        PetscPrintf(PETSC_COMM_WORLD, "No results. Initializing\n");

        mesh.setBoundaryCondition(elleft_zone->getRightBoundary(), &bcond_el[0]);

        app.pseudoTransient();
        cpright_zone->bvreaction = &shuttle;
        cpright_zone->updateVolumeProperties();
        cpleft_zone->bvreaction = &shuttle;
        cpleft_zone->updateVolumeProperties();

        int bcond_none[] = {-1, -1, -1, -1, -1, -1, -1, -1, -1};
        mesh.setBoundaryCondition(elleft_zone->getRightBoundary(), &bcond_none[0]);

        app.transient(1e0, 1e-3);
        app.transient(1e3, 5e-1);
    }

    if (ocp)
    {
        int bcond_opencircuit[] = {2, 2, 2, 2, 2, 2, 2, 2, 2};
        mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_opencircuit[0]);
        app.transient(simtime, 1e-1);
    }
    else
    {
        mesh.modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, n - 2);
        app.transient(simtime, (simtime > 1e-3) ? 1e-3 : simtime);
    }

    ResultWriter rw("outdata/" + name, &app);
    rw.write_state(0);
    rw.close();

    // // Self discharge
    // auto SD = [&] (double dV, double dt, double step = 1e-1, double programtime = 50, double programstep = 1e-1) {
    //     double bias = dV;
    //     int bcond_opencircuit[] = {2, 2, 2, 2, 2, 2, 2, 2, 2};
    //     mesh.modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, n-2);
    //     app.pulse = dV;
    //     app.mode = "write";
    //     app.transient(programtime, 1e-1);
    //     mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_opencircuit[0]);
    //     app.pulse = 0;
    //     app.mode = "open";
    //     app.transient(dt, step);
    //     mesh.setBoundaryCondition(cpleft_zone->getLeftBoundary(), &bcond_cp[0]);
    // };
    // ResultWriter rwsdn("outdata/ChargingComparisonTSAPI", &app);
    // app.rw = &rwsdn;
    // rwsdn.fullWrite=false;
    // SD(0,80,1e-1,10);
    // SD(20,1e4,1e-1,170,5e-2);
    // rwsdn.close();

    /* Finish and exit */
    app.destroy();
    return PetscFinalize();
}
